#version 450
#extension GL_EXT_nonuniform_qualifier : enable



layout (location = 0) in vec3 v2fPosition;
layout (location = 1) in vec3 v2fColor;
layout (location = 2) in vec2 v2fTexCoord;
layout (location = 3) flat in int v2fTexID;
layout (location = 4) in vec3 v2fNormal;


layout (set = 1, binding = 0) uniform sampler2D uTexColor[]; 


layout (location = 0) out vec4 fragColor;

void main()
{
	if (v2fTexID >= 0) {
		//int maxLOD = textureQueryLevels(uTexColor[nonuniformEXT(v2fTexID)]) - 1;
		//fragColor = vec4(textureLod(uTexColor[nonuniformEXT(v2fTexID)], v2fTexCoord, float(maxLOD - 2)).rgb, 1.);

		fragColor = texture(uTexColor[nonuniformEXT(v2fTexID)], v2fTexCoord).rgba;
		//fragColor = texture(uTexColor[v2fTexID], v2fTexCoord).rgba;
		//fragColor = vec4(v2fTexCoord, 0., 1.);
		//fragColor = vec4(v2fColor, 1.);
		return;
	}
	else {
		
		fragColor = vec4(v2fColor, 1.);
		return;
	}
		
	
	//fragColor = vec4(v2fNormal, 1.);

}
