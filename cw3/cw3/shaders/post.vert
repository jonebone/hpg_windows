#version 450

// scene info:
layout (set = 0, binding = 0) uniform UScene {
	mat4 camera;
	mat4 projection;
	mat4 projCam;
	vec4 cameraPos; // hid the number of lights as the 4th term
	vec4 ambientLightColor;
	vec4 resolution;
} uScene;

layout (location = 0) out vec2 UV;

const vec2 quadVertexPositions[4] = vec2[4] (
	// v1	v2
	// v3	v4 
	vec2(-1.,  1.),
	vec2( 1.,  1.),
	vec2(-1., -1.),
	vec2( 1., -1.)
);

const vec2 quadVertexUVs[4] = vec2[4] (
	// v1	v2
	// v3	v4 
	vec2( 0.,  1.),
	vec2( 1.,  1.),
	vec2( 0.,  0.),
	vec2( 1.,  0.)
);

void main()
{
	const vec2 xy = quadVertexPositions[gl_VertexIndex];
	gl_Position = vec4(xy, 0.5f, 1.f);
	UV = quadVertexUVs[gl_VertexIndex];
}
