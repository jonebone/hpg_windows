#version 450 core
#extension GL_EXT_nonuniform_qualifier : enable

#define M_PI 3.14159265358979323

layout (location = 0) in vec3 v2fPosition;
layout (location = 1) in vec2 v2fTexCoord;
layout (location = 2) flat in int v2fTexID;
layout (location = 3) in vec3 v2fNormal;

// scene info:
layout (set = 0, binding = 0) uniform UScene {
	mat4 camera;
	mat4 projection;
	mat4 projCam;
	vec4 cameraPos; // hid the number of lights as the 4th term
	vec4 ambientLightColor;
	vec4 resolution;
} uScene;


// PBR material:
layout( set = 1, binding = 0 ) uniform UMaterial
{
	vec4 emissive;
	vec4 albedo;
	float shininess;
	float metalness;
} uMaterial[]; // array of materials since I can do dynamic indexing

// tex array
layout( set = 2, binding = 0 ) uniform sampler2D uDiffuseMap[];



layout (location = 0) out vec4 fragColor;
layout (location = 1) out vec4 secondColor;



void main()
{
	vec3 nDir = normalize(v2fNormal);
	vec4 albedo = uMaterial[nonuniformEXT(v2fTexID)].albedo.rgba;
	float metalness = uMaterial[nonuniformEXT(v2fTexID)].metalness;
	float shininess = uMaterial[nonuniformEXT(v2fTexID)].shininess;

	vec2 uv = vec2(gl_FragCoord) / uScene.resolution.xy;

	//if (uv.x < 0.5) {
	//	fragColor = texture(uDiffuseMap[nonuniformEXT(v2fTexID)], v2fTexCoord).rgba;
	//	return;
	//} else {
	//	fragColor = vec4(uMaterial[nonuniformEXT(v2fTexID)].albedo.rgb, 1.);
	//	return;
	//}

	//fragColor = vec4(uMaterial[nonuniformEXT(v2fTexID)].emissive.rgb, 1.);
	//fragColor += albedo * uScene.ambientLightColor;

	fragColor = albedo;

	secondColor = vec4((nDir + 1.)/2., 0.);
}