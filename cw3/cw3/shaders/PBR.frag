#version 450
#extension GL_EXT_nonuniform_qualifier : enable

#define M_PI 3.14159265358979323

layout (location = 0) in vec3 v2fPosition;
layout (location = 1) in vec2 v2fTexCoord;
layout (location = 2) flat in int v2fTexID;
layout (location = 3) in vec3 v2fNormal;

// scene info:
layout (set = 0, binding = 0) uniform UScene {
	mat4 camera;
	mat4 projection;
	mat4 projCam;
	vec4 cameraPos; // hid the number of lights as the 4th term
	vec4 ambientLightColor;
	vec4 resolution;
} uScene;


// PBR material:
layout( set = 1, binding = 0 ) uniform UMaterial
{
	vec4 emissive;
	vec4 albedo;
	float shininess;
	float metalness;
} uMaterial[]; // array of materials since I can do dynamic indexing

// need a second set for lights since you can't have dynamic arrays in the same set
// lights :
layout( set = 2, binding = 0 ) uniform ULight
{
	vec4 position;
	vec4 color;
} uLight[]; // dynamic array of lights

// tex array
layout( set = 3, binding = 0 ) uniform sampler2D uDiffuseMap[];





layout (location = 0) out vec4 fragColor;

vec3 specular_base(vec4 col, float M) {
	return (1. - M) * vec3(0.04) + M * col.rgb;
}

vec3 Fresnel_term(vec3 sb, vec3 h, vec3 v) {
	return sb + (vec3(1.) - sb) * pow((1. - dot(h, v)), 5.);
}

vec3 Lambertian_diffuse(vec3 F, float M, vec4 col) {
	return (col.rgb / M_PI) * (1. - F) * (1. - M);
}

float BP_distro(float shine, float NdotH) {
	return ((shine + 2.) / (2. * M_PI)) * max(0., pow(NdotH, shine));
}

float CT_mask_unclamped(vec3 l, vec3 v, vec3 h, vec3 n) {
	float mult = 2. * dot(n, h) / dot(v, h);
	return min(1., min(mult * dot(n, v), mult * dot(n, l) ) );
}

float CT_mask(vec3 v, vec3 h, float NdotV, float NdotL, float NdotH) {
	float VdotH = clamp(dot(v, h), 0., 1.);
	float mult = 2. * NdotH / dot(v, h);
	return min(1., min(mult * NdotV, mult * NdotL ) );
}

// modified Henyey-Greenstein phase function
//                   1           1 - g^2
// pHG(theta, g) = ---- ----------------------------
//                 4 PI    1 + g^2 - 2g cos(theta)^1.5
// g is an eccentricity

float HG_phase(vec3 aDir, vec3 bDir, float eccentricity) {
    float cos_theta = dot(aDir, bDir); 
	float cos_2theta = clamp(2. * cos_theta * cos_theta - 1., 0., 1.);

    return (1. - eccentricity * eccentricity) /
           (4. * M_PI * (1. + eccentricity * eccentricity - (2. * eccentricity * pow(cos_2theta, 1.5) )));
}

// Burley 2012 generalizes the Trowbridge-Reitz (TR) specular distribution function 
// and cleverly dubs this generalization 'Generalized-Trowbridge-Reitz' (GTR)
// the difference between the two is that while TR has an expression squared,
// GTR has that expression to the power of lambda
// Burley then observes that GTR produces the same results as Henyey-Greenstein
// for lambda = 1.5, and when the theta used in GTR is doubled for Henyey-Greenstein
// this is justified by saying that HG considers a sphere while TR/GTR considers a hemisphere
// and doubling theta can be taken to be extending from the hemisphere to the sphere
// the theta used in GTR is the angle between the half vector and normal  


// GGX distribution 
// same as TR
// using the Unreal implementation
// found here: https://projectionspace.wordpress.com/2016/05/04/ggx-distribution-bridging-the-gap-between-theory-and-implementation/

float GGX(vec3 n, vec3 h, float r) {

	float d = (dot(n , h) * dot(n , h) * (r * r - 1.) + 1.);
	return (r * r) / (M_PI * d * d);
}

vec4 light_contribution(int index, vec3 viewDir, vec3 nDir, vec3 sb, vec4 aCol, float NdotV, float metal, float shine) {
	vec3 lightDir = normalize(uLight[nonuniformEXT(index)].position.xyz - v2fPosition);

	vec4 lColor = uLight[nonuniformEXT(index)].color;
	
	vec3 hDir = normalize(lightDir + viewDir);
	float NdotH = clamp(dot(nDir, hDir), 0., 1.);
	float NdotL = clamp(dot(nDir, lightDir), 0., 1.);

	vec3 fresnel = Fresnel_term(sb, hDir, viewDir);
	vec3 lambertian = Lambertian_diffuse(fresnel, metal, aCol);
	float ctMask = CT_mask(viewDir, hDir, NdotV, NdotL, NdotH);

	//float distro = BP_distro(shine, NdotH);
	float roughness = 1.697 / sqrt(shine + 4); // based on GGX paper https://www.cs.cornell.edu/~srm/publications/EGSR07-btdf.pdf
	// using 1-roughness for eccentricity
	float distro = HG_phase(nDir, hDir, (1. - roughness));
	//float distro = GGX(nDir, hDir, roughness);
	
	
	// don't clamp in division term so that we avoid div by zero
	return lColor * vec4(lambertian + (distro * fresnel * ctMask)/(4. * dot(nDir, viewDir) * dot(nDir, lightDir) + 0.001), 1.) * NdotL;
}

void main()
{
	vec3 viewDir = normalize(uScene.cameraPos.xyz - v2fPosition);
	vec3 nDir = normalize(v2fNormal);
	vec4 albedo = uMaterial[nonuniformEXT(v2fTexID)].albedo.rgba;
	float metalness = uMaterial[nonuniformEXT(v2fTexID)].metalness;
	float shininess = uMaterial[nonuniformEXT(v2fTexID)].shininess;
	float NdotV = clamp(dot(nDir, viewDir), 0., 1.);

	vec2 uv = vec2(gl_FragCoord) / uScene.resolution.xy;

	//if (uv.x < 0.5) {
	//	fragColor = texture(uDiffuseMap[nonuniformEXT(v2fTexID)], v2fTexCoord).rgba;
	//	return;
	//} else {
	//	fragColor = vec4(uMaterial[nonuniformEXT(v2fTexID)].albedo.rgb, 1.);
	//	return;
	//}
		

	vec3 sb = specular_base(albedo, metalness);

	fragColor = vec4(uMaterial[nonuniformEXT(v2fTexID)].emissive.rgb, 1.);
	fragColor += albedo * uScene.ambientLightColor;

	//fragColor = vec4(0.);

	// hid the number of lights as the 4th term of camera pos
	// use with max so that in 0 case we have 1 still
	int lightCount = max( 1, int(uScene.cameraPos.w)); 
	//lightCount = 3;
	
	//for (int j = 0; j < 10; j ++)
	for (int i = 0; i < lightCount; i++) {
		fragColor += light_contribution(i, viewDir, nDir, sb, albedo, NdotV, metalness, shininess);
	}
	// fragColor += everything else;


}