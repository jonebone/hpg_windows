#version 450
#extension GL_EXT_nonuniform_qualifier : enable

#define M_PI 3.14159265358979323

layout (location = 0) in vec3 v2fPosition;
layout (location = 1) in vec3 v2fColor;
layout (location = 2) in vec2 v2fTexCoord;
layout (location = 3) flat in int v2fTexID;
layout (location = 4) in vec3 v2fNormal;

// scene info:
layout (set = 0, binding = 0) uniform UScene {
	mat4 camera;
	mat4 projection;
	mat4 projCam;
	vec4 cameraPos;
	vec4 ambientLightColor;
} uScene;


// Blinn-Phong material:
layout( set = 1, binding = 0 ) uniform UMaterial
{
	vec4 emissive;
	vec4 diffuse;
	vec4 specular;
	float shininess; // Last to make std140 alignment easier.
} uMaterial[]; // array of materials since I can do dynamic indexing

// lights :
layout( set = 2, binding = 0 ) uniform ULight
{
	vec4 position;
	vec4 color;
} uLight[]; // dynamic array of lights

layout (location = 0) out vec4 fragColor;

vec4 light_contribution(int index, vec3 viewDir, vec3 nDir, vec4 diffuse, vec4 specular, float shine) {
	vec4 lColor = uLight[nonuniformEXT(index)].color;
	vec3 lightDir = normalize(uLight[nonuniformEXT(index)].position.xyz - v2fPosition);
	vec3 hDir = normalize(lightDir + viewDir);
	float NdotL = clamp(dot(nDir, lightDir), 0., 1.);
	float shineTerm = (shine + 2.)/8.;
	float NdotH = clamp(dot(nDir, hDir), 0., 1.);

	return lColor * NdotL * ((diffuse / M_PI) + shineTerm * max(0., pow(NdotH, shine)) * specular);
}

void main()
{
	vec3 viewDir = normalize(uScene.cameraPos.xyz - v2fPosition);
	vec3 nDir = normalize(v2fNormal);
	vec4 diffuseCol = uMaterial[nonuniformEXT(v2fTexID)].diffuse.rgba;
	vec4 specularCol = uMaterial[nonuniformEXT(v2fTexID)].specular.rgba;
	float shininess = uMaterial[nonuniformEXT(v2fTexID)].shininess;

	fragColor = vec4(uMaterial[nonuniformEXT(v2fTexID)].emissive.rgb, 1.);
	fragColor += diffuseCol * uScene.ambientLightColor;

	int lightCount = int(uScene.cameraPos.w); // hid the number of lights as the 4th term

	for (int i = 0; i < lightCount; i++) {
		fragColor += light_contribution(i, viewDir, nDir, diffuseCol, specularCol, shininess);
	}
	// fragColor += everything else;


}
