#version 450 core
#extension GL_EXT_nonuniform_qualifier : enable

#define M_PI 3.14159265358979323

layout (location = 0) in vec2 UV;

// scene info:
layout (set = 0, binding = 0) uniform UScene {
	mat4 camera;
	mat4 projection;
	mat4 projCam;
	vec4 cameraPos; // hid the number of lights as the 4th term
	vec4 ambientLightColor;
	vec4 resolution;
} uScene;

//// for use with combined sampler or image sampler typed descriptors
//layout(set = 1, binding = 0) uniform sampler2D colorMap;
//layout(set = 1, binding = 1) uniform sampler2D depthMap;
//layout(set = 1, binding = 2) uniform sampler2D normsMap;

// for use with input attachment type descriptors
layout (input_attachment_index = 0, set = 1, binding = 0) uniform subpassInput colorMap;
layout (input_attachment_index = 1, set = 1, binding = 1) uniform subpassInput depthMap;
layout (input_attachment_index = 2, set = 1, binding = 2) uniform subpassInput normsMap;


// need a second set for lights since you can't have dynamic arrays in the same set
// lights :
layout( set = 2, binding = 0 ) uniform ULight
{
	vec4 position;
	vec4 color;
} uLight[]; // dynamic array of lights



layout (location = 0) out vec4 fragColor;



void main()
{
//// for use w samplers
//	fragColor = texture(colorMap, UV).rgba;
//	if (UV.x > 0.33 ) fragColor = vec4(1. - texture(depthMap, UV).r);
//	if (UV.x > 0.66 ) fragColor = texture(normsMap, UV).rgba;

//// for use w input attachments
	fragColor = subpassLoad(colorMap).rgba;
	if (UV.x > 0.33 ) fragColor = vec4(1. - subpassLoad(depthMap).r);
	if (UV.x > 0.66 ) fragColor = subpassLoad(normsMap).rgba;

}