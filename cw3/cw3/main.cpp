﻿#include <volk/volk.h>

#include <tuple>
#include <chrono>
#include <limits>
#include <vector>
#include <stdexcept>

#include <cstdio>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstring>

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#if !defined(GLM_FORCE_RADIANS)
#	define GLM_FORCE_RADIANS
#endif
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "../labutils/to_string.hpp"
#include "../labutils/vulkan_window.hpp"

#include "../labutils/angle.hpp"
using namespace labutils::literals;

#include "../labutils/error.hpp"
#include "../labutils/vkutil.hpp"
#include "../labutils/vkimage.hpp"
#include "../labutils/vkobject.hpp"
#include "../labutils/vkbuffer.hpp"
#include "../labutils/allocator.hpp" 
namespace lut = labutils;

#include "model.hpp"

namespace
{
	namespace cfg
	{

		constexpr glm::vec4 ambientLight = glm::vec4(0.05);
		// Compiled shader code for the graphics pipeline(s)
		// See sources in cw2/shaders/*. 
#		define SHADERDIR_ "assets/cw3/shaders/"
		//constexpr char const* kVertShaderPath = SHADERDIR_ "default.vert.spv";
		//constexpr char const* kFragShaderPath = SHADERDIR_ "default.frag.spv";

		//constexpr char const* kVertShaderPath = SHADERDIR_ "BlinnPhong.vert.spv";
		//constexpr char const* kFragShaderPath = SHADERDIR_ "BlinnPhong.frag.spv";

		constexpr char const* kVertShaderPath = SHADERDIR_ "PBR.vert.spv";
		constexpr char const* kFragShaderPath = SHADERDIR_ "PBR.frag.spv";

		constexpr char const* kPreVertPath = SHADERDIR_ "pre.vert.spv";
		constexpr char const* kPreFragPath = SHADERDIR_ "pre.frag.spv";

		constexpr char const* kPostVertPath = SHADERDIR_ "post.vert.spv";
		constexpr char const* kPostFragPath = SHADERDIR_ "post.frag.spv";
#		undef SHADERDIR_

#		define ASSERTDIR_ "assets/cw3/"
		constexpr char const* kSponzModel = ASSERTDIR_ "sponza.obj";
		constexpr char const* kShipModel = ASSERTDIR_ "NewShip.obj";
		constexpr char const* kTestModel = ASSERTDIR_ "materialtest.obj";
#		undef ASSERTDIR_


		// General rule: with a standard 24 bit or 32 bit float depth buffer,
		// you can support a 1:1000 ratio between the near and far plane with
		// minimal depth fighting. Larger ratios will introduce more depth
		// fighting problems; smaller ratios will increase the depth buffer's
		// resolution but will also limit the view distance.
		constexpr float kCameraNear  = 0.1f;
		constexpr float kCameraFar = 100.f;
		//constexpr float kCameraFar   = 20000.f;

		constexpr auto kCameraFov    = 60.0_degf;

		// depth buffer format
		constexpr VkFormat kDepthFormat = VK_FORMAT_D32_SFLOAT;
		constexpr VkFormat kColorBufferFormat = VK_FORMAT_R8G8B8A8_SRGB;

		// light count
		constexpr uint32_t kLightCount = 12;
		// light positions
		constexpr glm::vec3 kLightPositions[] =
		{
			glm::vec3(0., 9.3, -3.),
			glm::vec3(-70., 10., 0.),
			glm::vec3(0., 10., 70.),
			glm::vec3(70., 10., 0.),
			glm::vec3(0., 1., 0.),
			glm::vec3(10., 10., 0.),
			glm::vec3(-10., 10., 0.),
			glm::vec3(0., -1., 10.),
			glm::vec3(12., 18., 43.),
			glm::vec3(82., 27., 1.),
			glm::vec3(-5.),
			glm::vec3(3., 5., 8.),
			glm::vec3(8., 5., 3.)
		};
		// light colors
		constexpr glm::vec4 kLightColors[] =
		{
			glm::vec4(0.7),
			glm::vec4(0.1, 0.1, 0.6, 1.),
			glm::vec4(0.1, 0.4, 0.1, 1.),
			glm::vec4(0.3, 0.2, 0.1, 1.),
			glm::vec4(1.),
			glm::vec4(1., .6, .2, 1.),
			glm::vec4(.5, .75, 1., 1.),
			glm::vec4(0.2, 0.2, 0.3, 1.),
			glm::vec4(0.3, 0.5, 0.1, 1.),
			glm::vec4(0.2, 0.7, 0.6, 1.),
			glm::vec4(0.1, 0.4, 0.4, 1.),
			glm::vec4(0.3, 0.5, 0.1, 1.)
		};

	}

	// Local types/structures:
	
	namespace glsl
	{
		// vec3 = 12 bytes, 16 byte aligned 
		// vec4 = 16 bytes, 16 byte aligned
		// float=  4 bytes,  4 byte aligned
		// vec3/4 must start on 16 byte bound
		// but vec3 ONLY consumes 12 bytes
		// just use vec4 instead

		// lighting uniform struct
		struct LightingUniform
		{
			glm::vec4 lightPos;
			glm::vec4 lightColor;

		};
	}
	
	// For Blinn Phong (see BlinnPhong.frag):
	//
	//namespace glsl
	//{
	//	struct MaterialUniform
	//	{
	//		// Note: must map to the std140 uniform interface in the fragment
	//		// shader, so need to be careful about the packing/alignment here!
	//		glm::vec4 emissive;
	//		glm::vec4 diffuse;
	//		glm::vec4 specular;
	//		float shininess;
	//	};
	//}
	
	// For PBR (see PBR.frag):
	
	namespace glsl
	{
		struct MaterialUniform
		{
			// Note: must map to the std140 uniform interface in the fragment
			// shader, so need to be careful about the packing/alignment here!
			glm::vec4 emissive;
			glm::vec4 albedo;
			float shininess;
			float metalness;
		};
	}

	// for both: (see default.frag)
	//namespace glsl
	//{
	//	struct MaterialUniform
	//	{
	//		// Note: must map to the std140 uniform interface in the fragment
	//		// shader, so need to be careful about the packing/alignment here!
	//		glm::vec4 emissive;
	//		glm::vec4 albedo;
	//		glm::vec4 specular;
	//		float shininess;
	//		float metalness;
	//	};
	//}

	

	// GLFW callbacks
	void glfw_callback_key_press(GLFWwindow*, int, int, int, int);
	void glfw_callback_cursor_position(GLFWwindow*, double xpos, double ypos);
	void glfw_callback_mouse_button(GLFWwindow*, int button, int action, int mods);

	// camera vars
	bool mousecam_on = false;
	double lastX = 0., lastY = 0.;

	float xSensitivity = 0.2;
	float ySensitivity = 0.2;

	float cPitch = 0.f, cYaw = 0.f;
	// camera vecs
	glm::vec3 camera_pos = glm::vec3(0.f, 0.f, 0.f);
	glm::vec3 camera_dir = glm::vec3(1.f, 0.f, 0.f);
	glm::vec3 world_up = glm::vec3(0.f, 1.f, 0.f);

	// movespeed
	float velocity = 3.;

	// % of lights that are on 
	float plightsOn = 0.;

	// animation on/off as 1/0 respectively
	bool lanimated = true;

	// time
	double lastTime;
	double uTime = 0.;

	// screen resolution
	int windowWidth, windowHeight;


	// Uniform data
	namespace glsl
	{
		struct SceneUniform
		{
			// Note: need to be careful about the packing/alignment here!
			glm::mat4 camera;
			glm::mat4 projection;
			glm::mat4 projCam;

			glm::vec4 cameraPosition;
			glm::vec4 ambientColor;
			glm::vec4 resolution;
		};

		// We want to use vkCmdUpdateBuﬀer() to update the contents of our uniform
		// buﬀers vkCmdUpdateBuﬀer() has a number of requirements, including
		// the two below. See
		//  https://www.khronos.org/registry/vulkan/specs/1.3-extensions/man/html/vkCmdUpdateBuffer.html
		static_assert(sizeof(SceneUniform) <= 65536, "SceneUniform must be less than 65536 bytes for vkCmdUpdateBuffer");
		static_assert(sizeof(SceneUniform) % 4 == 0, "SceneUniform size must be a multiple of 4 bytes");

	}

	// Helpers:
	lut::RenderPass create_render_pass(lut::VulkanWindow const&);

	lut::DescriptorSetLayout create_scene_descriptor_layout(lut::VulkanWindow const&);
	lut::DescriptorSetLayout create_object_descriptor_layout(lut::VulkanWindow const&);
	// create model descriptor layout takes additional uint32_t for max descriptor sampler number
	lut::DescriptorSetLayout create_material_descriptor_layout(lut::VulkanWindow const&, uint32_t);
	lut::DescriptorSetLayout create_lights_descriptor_layout(lut::VulkanWindow const&, uint32_t);
	lut::DescriptorSetLayout create_texture_descriptor_layout(lut::VulkanWindow const&, uint32_t);
	lut::DescriptorSetLayout create_gbuffer_descriptor_layout(lut::VulkanWindow const&);

	lut::Pipeline create_prebuffer_pipeline(lut::VulkanWindow const&, VkRenderPass, VkPipelineLayout);
	lut::PipelineLayout create_prebuffer_pipeline_layout(lut::VulkanContext const&, VkDescriptorSetLayout, VkDescriptorSetLayout, VkDescriptorSetLayout);

	lut::Pipeline create_postbuffer_pipeline(lut::VulkanWindow const&, VkRenderPass, VkPipelineLayout);
	lut::PipelineLayout create_postbuffer_pipeline_layout(lut::VulkanContext const&, VkDescriptorSetLayout, VkDescriptorSetLayout, VkDescriptorSetLayout);

	std::tuple<lut::Image, lut::ImageView> create_depth_buffer(lut::VulkanWindow const&, lut::Allocator const&);
	// same as above but with a different format
	std::tuple<lut::Image, lut::ImageView> create_intermediate_buffer(lut::VulkanWindow const&, lut::Allocator const&);

	// at bottom of file:
	void load_blinn_buffer(VkBuffer, MaterialInfo, VkCommandBuffer const&); 
	void load_PBR_buffer(VkBuffer, MaterialInfo, VkCommandBuffer const&);
	void load_both_buffer(VkBuffer, MaterialInfo, VkCommandBuffer const&); 
	void load_light_buffer(VkBuffer, int, VkCommandBuffer const&); 
	void update_light_buffer(glm::mat4, VkBuffer, int, VkCommandBuffer const&);

	
	void create_swapchain_framebuffers(
		lut::VulkanWindow const&,
		VkRenderPass,
		std::vector<lut::Framebuffer>&,
		VkImageView aDepthView,
		VkImageView aColorView,
		VkImageView aNormsView
	);

	void update_scene_uniforms(
		glsl::SceneUniform&,
		//glsl::FragSceneUniform&,
		std::uint32_t aFramebufferWidth,
		std::uint32_t aFramebufferHeight
	);

	void record_commands(
		VkCommandBuffer,
		VkRenderPass,
		VkFramebuffer,
		VkPipeline,
		VkPipeline, // second pipeline for second pass
		VkExtent2D const&,
		// added for passing attributes to vertex shader:
		BufferMesh& aBuffers,
		//BufferMesh& bBuffers,
		// scene uniforms
		VkBuffer aSceneUBO,
		glsl::SceneUniform const&,
		// light uniform buffer vector
		std::vector<lut::Buffer> const&,
		std::vector<lut::Image*> const&,
		VkPipelineLayout,
		VkPipelineLayout, // for second pass
		VkDescriptorSet aSceneDescriptors,
		//VkDescriptorSet aModelDescriptors
		//VkDescriptorSet aFragSceneDescriptors,
		VkDescriptorSet aMaterialDescriptors,
		VkDescriptorSet aLightsDescriptors,
		VkDescriptorSet aTexDescriptors,
		VkDescriptorSet gBufferDescriptors
		//VkDescriptorSet bObjectjDescriptors
	);

	void submit_commands(
		lut::VulkanWindow const&,
		VkCommandBuffer,
		VkFence,
		VkSemaphore,
		VkSemaphore
	);

	void present_results(
		VkQueue,
		VkSwapchainKHR,
		std::uint32_t aImageIndex,
		VkSemaphore,
		bool& aNeedToRecreateSwapchain
	);
}




int main() try
{
	// load obj files
	//ModelData shipModel = load_obj_model(cfg::kShipModel);
	ModelData shipModel = load_obj_model(cfg::kTestModel);
	//ModelData shipModel = load_obj_model(cfg::kSponzModel);

	// Create Vulkan Window
	auto window = lut::make_vulkan_window();

	// get device properties so that we have values ready for descriptor layout and sampler anisotropic filtering
	VkPhysicalDeviceProperties deviceProperties{};
	vkGetPhysicalDeviceProperties(window.physicalDevice, &deviceProperties);

	// Configure the GLFW window
	glfwSetKeyCallback(window.window, &glfw_callback_key_press);
	glfwSetCursorPosCallback(window.window, &glfw_callback_cursor_position);
	glfwSetMouseButtonCallback(window.window, &glfw_callback_mouse_button);

	// Create VMA allocator
	lut::Allocator allocator = lut::create_allocator(window);

	// Intialize resources
	lut::RenderPass renderPass = create_render_pass(window);

	/// (Section 3) create scene descriptor set layout
	lut::DescriptorSetLayout sceneLayout = create_scene_descriptor_layout(window);

	/// (Section 4) create object descriptor set layout
	lut::DescriptorSetLayout materialsLayout = create_material_descriptor_layout(window, deviceProperties.limits.maxDescriptorSetUniformBuffers / 2); 
	lut::DescriptorSetLayout lightsLayout = create_lights_descriptor_layout(window, deviceProperties.limits.maxDescriptorSetUniformBuffers / 4); // div by 4 bc div by 3 is unreasonable
	lut::DescriptorSetLayout texLayout = create_texture_descriptor_layout(window, deviceProperties.limits.maxDescriptorSetSamplers - 10); // - 10 for safety
	lut::DescriptorSetLayout gBufferLayout = create_gbuffer_descriptor_layout(window);

	//std::printf("%ld \n", deviceProperties.limits.maxUniformBufferRange);
	//std::printf("%ld \n", deviceProperties.limits.maxDescriptorSetUniformBuffers);
	//std::printf("%ld \n", deviceProperties.limits.maxDescriptorSetUniformBuffers / 4);

	lut::PipelineLayout prepipeLayout = create_prebuffer_pipeline_layout(window, sceneLayout.handle, 
		materialsLayout.handle, texLayout.handle);
	lut::Pipeline prepipe = create_prebuffer_pipeline(window, renderPass.handle, prepipeLayout.handle);


	lut::PipelineLayout postpipeLayout = create_postbuffer_pipeline_layout(window, sceneLayout.handle,
		gBufferLayout.handle, lightsLayout.handle);
	lut::Pipeline postpipe = create_postbuffer_pipeline(window, renderPass.handle, postpipeLayout.handle);



	/// (Section 6) create depth buffer
	auto [depthBuffer, depthBufferView] = create_depth_buffer(window, allocator);
	auto [normBuffer, normBufferView] = create_intermediate_buffer(window, allocator);
	auto [diffuseBuffer, diffuseBufferView] = create_intermediate_buffer(window, allocator);

	std::vector<lut::Framebuffer> framebuffers;
	create_swapchain_framebuffers(window, renderPass.handle, framebuffers, 
		depthBufferView.handle,
		diffuseBufferView.handle,
		normBufferView.handle
	);

	lut::CommandPool cpool = lut::create_command_pool(window, VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT);

	std::vector<VkCommandBuffer> cbuffers;
	std::vector<lut::Fence> cbfences;

	for (std::size_t i = 0; i < framebuffers.size(); ++i)
	{
		cbuffers.emplace_back(lut::alloc_command_buffer(window, cpool.handle));
		cbfences.emplace_back(lut::create_fence(window, VK_FENCE_CREATE_SIGNALED_BIT));
	}

	lut::Semaphore imageAvailable = lut::create_semaphore(window);
	lut::Semaphore renderFinished = lut::create_semaphore(window);

	// Load data

	BufferMesh modelBuffers = create_buffer_mesh(shipModel, window, allocator);
	//BufferMesh cityMesh = create_buffer_mesh(cityModel, window, allocator);

	///(Section 3) create scene uniform buffer with lut::create_buffer()
	lut::Buffer sceneUBO = lut::create_buffer(
		allocator,
		sizeof(glsl::SceneUniform),
		VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		VMA_MEMORY_USAGE_GPU_ONLY
	);

	/// (Section 3) create descriptor pool
	lut::DescriptorPool dpool = lut::create_descriptor_pool(window);

	/// (Section 3) allocate descriptor set for uniform buffer
	/// (Section 3) initialize descriptor set with vkUpdateDescriptorSets
	VkDescriptorSet sceneDescriptors = lut::alloc_desc_set(window, dpool.handle, sceneLayout.handle);
	{

		VkDescriptorBufferInfo sceneUboInfo{};
		sceneUboInfo.buffer = sceneUBO.buffer;
		sceneUboInfo.range = VK_WHOLE_SIZE;

		VkWriteDescriptorSet desc[1]{};
		desc[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		desc[0].dstSet = sceneDescriptors;
		desc[0].dstBinding = 0;
		desc[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		desc[0].descriptorCount = 1;
		desc[0].pBufferInfo = &sceneUboInfo;
		constexpr auto numSets = sizeof(desc) / sizeof(desc[0]);
		vkUpdateDescriptorSets(window.device, numSets, desc, 0, nullptr);
	}


	//lut::Sampler defaultSampler = lut::create_default_sampler(window);
	lut::Sampler defaultSampler = lut::create_anisotropic_sampler(window, true, deviceProperties.limits.maxSamplerAnisotropy);
	/// (Section 4) allocate and initialize descriptor sets for materials
	// and eventually lights

	// material buffer prep
	lut::CommandPool loadCmdPool = lut::create_command_pool(window, VK_COMMAND_POOL_CREATE_TRANSIENT_BIT);
	// command buffer for copying material structs into buffers
	VkCommandBuffer uniformCmdBuff = lut::alloc_command_buffer(window, loadCmdPool.handle);

	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = 0;
	beginInfo.pInheritanceInfo = nullptr;

	if (auto const res = vkBeginCommandBuffer(uniformCmdBuff, &beginInfo); VK_SUCCESS != res)
	{
		throw lut::Error("Beginning command buffer recording\n"
			"vkBeginCommandBuffer() returned %s", lut::to_string(res).c_str());
	}

	// need vectors just so that they stay existing
	std::vector<lut::Buffer> matDescriptorBuffers = std::vector<lut::Buffer>();
	std::vector<lut::Buffer> lightDescriptorBuffers = std::vector<lut::Buffer>();
	VkDescriptorBufferInfo defaultBufferInfo{};
	std::vector<VkDescriptorBufferInfo> matBufferInfos = std::vector<VkDescriptorBufferInfo>(shipModel.materials.size(), defaultBufferInfo);
	std::vector<VkDescriptorBufferInfo> lightBufferInfos = std::vector<VkDescriptorBufferInfo>(cfg::kLightCount, defaultBufferInfo);
	// scene uniforms have their data written to their buffer in record_commands
	// we can write material data to material buffers before then since materials don't need updating

	// load all textures from model data and make imageviews from them
	int matCount = 0;
	for (MaterialInfo mi : shipModel.materials) {
		auto& mb = matDescriptorBuffers.emplace_back(lut::create_buffer(
			allocator,
			sizeof(glsl::MaterialUniform),
			VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			VMA_MEMORY_USAGE_GPU_ONLY
		));

		//load_blinn_buffer(mb.buffer, mi, uniformCmdBuff);
		load_PBR_buffer(mb.buffer, mi, uniformCmdBuff);

		matBufferInfos[matCount].buffer = mb.buffer;
		matBufferInfos[matCount].range = VK_WHOLE_SIZE;

		matCount++;
	} // end material loop

	//std::printf("matcount: %i\n", matCount);

	// light buffers loop
	for (int i = 0; i < cfg::kLightCount; i++) {
		auto& lb = lightDescriptorBuffers.emplace_back(lut::create_buffer(
			allocator,
			sizeof(glsl::LightingUniform),
			VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
			VMA_MEMORY_USAGE_GPU_ONLY
		));

		load_light_buffer(lb.buffer, i, uniformCmdBuff);

		
		lightBufferInfos[i].buffer = lb.buffer;
		lightBufferInfos[i].range = VK_WHOLE_SIZE;
	} // end lights loop

	// End command recording
	if (auto const res = vkEndCommandBuffer(uniformCmdBuff); VK_SUCCESS != res)
	{
		throw lut::Error("Ending command buffer recording\n"
			"vkEndCommandBuffer() returned %s", lut::to_string(res).c_str());
	}

	// Submit command buﬀer and wait for commands to complete
	// Commands must have completed before we can destroy the temporary
	// resources, such as the staging buﬀers.
	lut::Fence descriptorsBuffersCopyComplete = lut::create_fence(window);

	VkSubmitInfo submitBBInfo{};
	submitBBInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitBBInfo.commandBufferCount = 1;
	submitBBInfo.pCommandBuffers = &uniformCmdBuff;

	if (auto const res = vkQueueSubmit(window.graphicsQueue, 1, &submitBBInfo, descriptorsBuffersCopyComplete.handle); VK_SUCCESS != res)
	{
		throw lut::Error("Submitting commands\n"
			"vkQueueSubmit() returned %s", lut::to_string(res).c_str());
	}

	if (auto const res = vkWaitForFences(window.device, 1, &descriptorsBuffersCopyComplete.handle, VK_TRUE, std::numeric_limits<std::uint64_t>::max()); VK_SUCCESS != res)
	{
		throw lut::Error("Waiting for upload to complete\n"
			"vkWaitForFences() returned %s", lut::to_string(res).c_str());
	}

	//int numTex = imgViews.size();
	uint32_t modelDescCounts[] = {1, matCount, cfg::kLightCount};
	uint32_t uMatCount = matCount;

	/// allocate and initialize descriptor set for materials
	// alloc_desc_set now takes an additonal uint32_t* param to specify its number of descriptors
	VkDescriptorSet materialDescriptors = lut::alloc_desc_set(window, dpool.handle, materialsLayout.handle, 1, &uMatCount);
	{
		VkWriteDescriptorSet desc[1]{};
		desc[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		desc[0].dstSet = materialDescriptors;
		desc[0].dstBinding = 0;
		desc[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		desc[0].descriptorCount = matCount; // changed
		desc[0].pBufferInfo = matBufferInfos.data();

		constexpr auto numSets = sizeof(desc) / sizeof(desc[0]);
		vkUpdateDescriptorSets(window.device, numSets, desc, 0, nullptr);
	}

	uint32_t uLightCount = cfg::kLightCount;
	/// allocate and initialize descriptor set for lights
	// alloc_desc_set now takes an additonal uint32_t* param to specify its number of descriptors
	VkDescriptorSet lightsDescriptors = lut::alloc_desc_set(window, dpool.handle, lightsLayout.handle, 1, &uLightCount);
	{
		VkWriteDescriptorSet desc[1]{};
		desc[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		desc[0].dstSet = lightsDescriptors;
		desc[0].dstBinding = 0;
		desc[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		desc[0].descriptorCount = uLightCount; // changed
		desc[0].pBufferInfo = lightBufferInfos.data();

		constexpr auto numSets = sizeof(desc) / sizeof(desc[0]);
		vkUpdateDescriptorSets(window.device, numSets, desc, 0, nullptr);
	}

	// setting up textures
	// 
	// vectors of imageView pointers and image pointers allow us to avoid their premature destruction
	// but still have the pointers for cleanup later
	std::vector<lut::ImageView*> imgViews = std::vector<lut::ImageView*>();
	std::vector<lut::Image*> imgs = std::vector<lut::Image*>();

	// populate vectors
	create_teximg_vectors(imgs, imgViews, shipModel,
		window, loadCmdPool.handle, allocator);

	uint32_t texCount = imgs.size();

	VkDescriptorSet texDescriptors = lut::alloc_desc_set(window, dpool.handle, texLayout.handle, 1, &texCount);
	{
		VkWriteDescriptorSet desc[1]{};

		// create textureInfo as default values for ImageInfo
		VkDescriptorImageInfo textureInfo{};
		textureInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		textureInfo.sampler = defaultSampler.handle;

		std::vector<VkDescriptorImageInfo> imgInfos = std::vector<VkDescriptorImageInfo>(texCount, textureInfo);
		for (int i = 0; i < texCount; i++) {
			// all other vals are set by default already
			imgInfos[i].imageView = imgViews[i]->handle;
		}


		desc[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		desc[0].dstSet = texDescriptors;
		desc[0].dstBinding = 0;
		desc[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		desc[0].descriptorCount = texCount; // change
		desc[0].pImageInfo = imgInfos.data(); // change to aray of textureInfos

		constexpr auto numSets = sizeof(desc) / sizeof(desc[0]);
		vkUpdateDescriptorSets(window.device, numSets, desc, 0, nullptr);
	}
	

	// defferred descriptors
	std::vector<lut::ImageView*> GBuffers{
		&diffuseBufferView,
		&depthBufferView,
		&normBufferView
	};

	std::vector<lut::Image*> gBufferImgs{
		&depthBuffer, // key to start with depth buffer here
		&diffuseBuffer,
		&normBuffer
	};
	
	uint32_t GBufferCount = 3;

	VkDescriptorSet gDescriptors = lut::alloc_desc_set(window, dpool.handle, gBufferLayout.handle);
	{
		// create textureInfo as default values for ImageInfo
		VkDescriptorImageInfo textureInfo{};
		textureInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		//textureInfo.sampler = defaultSampler.handle;
		textureInfo.sampler = VK_NULL_HANDLE;

		std::vector<VkDescriptorImageInfo> imgInfos = std::vector<VkDescriptorImageInfo>(GBufferCount, textureInfo);
		for (int i = 0; i < GBufferCount; i++) {
			// all other vals are set by default already
			imgInfos[i].imageView = GBuffers[i]->handle;
		}
		//imgInfos[1].imageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;

		VkWriteDescriptorSet desc[3]{};
		desc[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		desc[0].dstSet = gDescriptors;
		desc[0].dstBinding = 0;
		//desc[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER; // changed
		desc[0].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT; // changed
		desc[0].descriptorCount = 1; // changed to 1
		desc[0].pImageInfo = &imgInfos[0];
		
		desc[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		desc[1].dstSet = gDescriptors;
		desc[1].dstBinding = 1;
		//desc[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER; // changed
		desc[1].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT; // changed
		desc[1].descriptorCount = 1; // changed to 1
		desc[1].pImageInfo = &imgInfos[1];
		
		desc[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		desc[2].dstSet = gDescriptors;
		desc[2].dstBinding = 2;
		//desc[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER; // changed
		desc[2].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT; // changed
		desc[2].descriptorCount = 1; // changed to 1
		desc[2].pImageInfo = &imgInfos[2];

		constexpr auto numSets = sizeof(desc) / sizeof(desc[0]);
		vkUpdateDescriptorSets(window.device, numSets, desc, 0, nullptr);
	}



	// Application main loop
	bool recreateSwapchain = false;

	// start time
	glfwSetTime(0.);
	lastTime = glfwGetTime();

	while (!glfwWindowShouldClose(window.window))
	{
		// Let GLFW process events.
		// glfwPollEvents() checks for events, processes them. If there are no
		// events, it will return immediately. Alternatively, glfwWaitEvents()
		// will wait for any event to occur, process it, and only return at
		// that point. The former is useful for applications where you want to
		// render as fast as possible, whereas the latter is useful for
		// input-driven applications, where redrawing is only needed in
		// reaction to user input (or similar).
		glfwPollEvents(); // or: glfwWaitEvents()

		glfwGetFramebufferSize(window.window, &windowWidth, &windowHeight);

		// Recreate swap chain?
		if (recreateSwapchain)
		{
			// We need to destroy several objects, which may still be in use by
			// the GPU. Therefore, ﬁrst wait for the GPU to ﬁnish processing.
			vkDeviceWaitIdle(window.device);
			// Recreate them
			auto const changes = recreate_swapchain(window);

			if (changes.changedFormat)
				renderPass = create_render_pass(window);

			if (changes.changedSize)
				std::tie(depthBuffer, depthBufferView) = create_depth_buffer(window, allocator);

			framebuffers.clear();
			create_swapchain_framebuffers(window, renderPass.handle, framebuffers,
				depthBufferView.handle,
				diffuseBufferView.handle,
				normBufferView.handle
			);

			if (changes.changedSize) {
				prepipe = create_prebuffer_pipeline(window, renderPass.handle, prepipeLayout.handle);
				postpipe = create_postbuffer_pipeline(window, renderPass.handle, postpipeLayout.handle);

			}

			recreateSwapchain = false;
		}

		// pasted from ex3
		/// acquire swapchain image.
		// Acquire next swap chain image
		std::uint32_t imageIndex = 0;
		auto const acquireRes = vkAcquireNextImageKHR(
			window.device,
			window.swapchain,
			std::numeric_limits<std::uint64_t>::max(),
			imageAvailable.handle,
			VK_NULL_HANDLE,
			&imageIndex
		);

		if (VK_SUBOPTIMAL_KHR == acquireRes || VK_ERROR_OUT_OF_DATE_KHR == acquireRes)
		{
			// This occurs e.g., when the window has been resized. In this case
			// we need to recreate the swap chain to match the new dimensions.
			// Any resources that directly depend on the swap chain need to be
			// recreated as well. While rare, re-creating the swap chain may
			// give us a diﬀerent image format, which we should handle.
			//
			// In both cases, we set the ﬂag that the swap chain has to be
			// re-created and jump to the top of the loop. Technically, with
			// the VK SUBOPTIMAL KHR return code, we could continue rendering
			// with the current swap chain (unlike VK ERROR OUT OF DATE KHR,
			// which does require us to recreate the swap chain).
			recreateSwapchain = true;
			continue;
		}

		if (VK_SUCCESS != acquireRes)
		{
			throw lut::Error("Unable to acquire enxt swapchain image\n"
				"vkAcquireNextImageKHR() returned % s", lut::to_string(acquireRes).c_str());
		}

		/// wait for command buffer to be available
		// Make sure that the command buﬀer is no longer in use
		assert(std::size_t(imageIndex) < cbfences.size());

		if (auto const res = vkWaitForFences(window.device, 1, &cbfences[imageIndex].handle, VK_TRUE, std::numeric_limits<std::uint64_t>::max()); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to wait for command buffer fence %u\n"
				"vkWaitForFences() returned % s", imageIndex, lut::to_string(res).c_str());
		}

		if (auto const res = vkResetFences(window.device, 1, &cbfences[imageIndex].handle); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to reset command buffer fence %u\n"
				"vkResetFences() returned % s", imageIndex, lut::to_string(res).c_str());
		}

		/// Section 3 p18 just says 'add to main loop'
		// here, since it's after swapchain image is for sure available and after command buffer is available
		// but before shaders are set up
		glsl::SceneUniform sceneUniforms{};
		sceneUniforms.ambientColor = cfg::ambientLight; // set ambient value early

		update_scene_uniforms(sceneUniforms, window.swapchainExtent.width, window.swapchainExtent.height);

		/// record and submit commands
		// Record and submit commands for this frame
		assert(std::size_t(imageIndex) < cbuffers.size());
		assert(std::size_t(imageIndex) < framebuffers.size());
		record_commands(
			cbuffers[imageIndex],
			renderPass.handle,
			framebuffers[imageIndex].handle,
			prepipe.handle,
			postpipe.handle,
			window.swapchainExtent,
			// add'l params for vertex shader attributes:
			modelBuffers,
			// scene uniforms
			sceneUBO.buffer,
			sceneUniforms,
			// light buffers vector
			lightDescriptorBuffers,
			gBufferImgs,
			prepipeLayout.handle,
			postpipeLayout.handle,
			sceneDescriptors,
			materialDescriptors,
			lightsDescriptors,
			texDescriptors,
			gDescriptors
			//fragSceneDescriptors,
			//materialDescriptors,
			//materialDescriptors
		);

		submit_commands(
			window,
			cbuffers[imageIndex],
			cbfences[imageIndex].handle,
			imageAvailable.handle,
			renderFinished.handle
		);
		// end paste from ex3

		/// (Section 1)  - present rendered images (note: use the present_results() method)
		present_results(window.presentQueue, window.swapchain, imageIndex, renderFinished.handle, recreateSwapchain);

	}

	// Cleanup takes place automatically in the destructors, but we sill need
	// to ensure that all Vulkan commands have finished before that.

	vkDeviceWaitIdle(window.device);

	cleanup_teximg_vectors(imgs, imgViews);

	return 0;
}

catch (std::exception const& eErr)
{
	std::fprintf(stderr, "\n");
	std::fprintf(stderr, "Error: %s\n", eErr.what());
	return 1;
}

// glfw callbacks
namespace
{
	void glfw_callback_key_press(GLFWwindow* aWindow, int aKey, int /*aScanCode*/, int aAction, int /*aModifierFlags*/)
	{
		if (GLFW_KEY_ESCAPE == aKey && GLFW_PRESS == aAction)
		{
			glfwSetWindowShouldClose(aWindow, GLFW_TRUE);
			return;
		}



		if (GLFW_KEY_LEFT_SHIFT == aKey && GLFW_PRESS == aAction)
		{
			velocity = 10.;
		}
		else if (GLFW_KEY_LEFT_CONTROL == aKey && GLFW_PRESS == aAction)
		{
			velocity = .5;
		}

		if ((GLFW_KEY_LEFT_SHIFT == aKey || GLFW_KEY_LEFT_CONTROL == aKey) && GLFW_RELEASE == aAction)
		{
			velocity = 3.;
		}

		glm::vec3 fwd;
		fwd.x = cos(glm::radians(cYaw)) * cos(glm::radians(cPitch));
		fwd.z = sin(glm::radians(cYaw)) * cos(glm::radians(cPitch));
		fwd.y = sin(glm::radians(cPitch));
		fwd = glm::normalize(fwd);
		camera_dir = fwd;

		glm::vec3 left = glm::normalize(glm::cross(world_up, fwd));
		fwd = glm::normalize(glm::cross(world_up, left));



		if (GLFW_KEY_W == aKey && (GLFW_PRESS == aAction || GLFW_REPEAT == aAction))
		{
			camera_pos -= fwd * velocity;
		}
		if (GLFW_KEY_S == aKey && (GLFW_PRESS == aAction || GLFW_REPEAT == aAction))
		{
			camera_pos += fwd * velocity;
		}

		if (GLFW_KEY_A == aKey && (GLFW_PRESS == aAction || GLFW_REPEAT == aAction))
		{
			camera_pos += left * velocity;
		}
		if (GLFW_KEY_D == aKey && (GLFW_PRESS == aAction || GLFW_REPEAT == aAction))
		{
			camera_pos -= left * velocity;
		}

		if (GLFW_KEY_Q == aKey && (GLFW_PRESS == aAction || GLFW_REPEAT == aAction))
		{
			camera_pos += world_up * velocity;
		}
		if (GLFW_KEY_E == aKey && (GLFW_PRESS == aAction || GLFW_REPEAT == aAction))
		{
			camera_pos -= world_up * velocity;
		}

		/// lighting responses

		// animation on off
		if (GLFW_KEY_SPACE == aKey && GLFW_PRESS == aAction) lanimated = !lanimated;

		// num lights
		if (GLFW_KEY_1 == aKey && GLFW_PRESS == aAction) plightsOn = 0.; // 0 so we can know to set exactly 1
		if (GLFW_KEY_2 == aKey && GLFW_PRESS == aAction) plightsOn = 0.2;
		if (GLFW_KEY_3 == aKey && GLFW_PRESS == aAction) plightsOn = 0.3;
		if (GLFW_KEY_4 == aKey && GLFW_PRESS == aAction) plightsOn = 0.4;
		if (GLFW_KEY_5 == aKey && GLFW_PRESS == aAction) plightsOn = 0.5;
		if (GLFW_KEY_6 == aKey && GLFW_PRESS == aAction) plightsOn = 0.6;
		if (GLFW_KEY_7 == aKey && GLFW_PRESS == aAction) plightsOn = 0.7;
		if (GLFW_KEY_8 == aKey && GLFW_PRESS == aAction) plightsOn = 0.8;
		if (GLFW_KEY_9 == aKey && GLFW_PRESS == aAction) plightsOn = 0.9;
		if (GLFW_KEY_0 == aKey && GLFW_PRESS == aAction) plightsOn = 1.0; // all lights on 

	}

	void glfw_callback_cursor_position(GLFWwindow* window, double xpos, double ypos) {
		if (!mousecam_on) return;
		double xoffset, yoffset;
		xoffset = xpos - lastX;
		yoffset = ypos - lastY;
		// update lastX and lastY
		lastX = xpos;
		lastY = ypos;

		cYaw += xoffset * xSensitivity;
		cPitch -= yoffset * ySensitivity;

		// make sure we dont flip
		if (cPitch > 89.0f)
			cPitch = 89.0f;
		if (cPitch < -89.0f)
			cPitch = -89.0f;

		// update camera
		glm::vec3 fwd;
		fwd.x = cos(glm::radians(cYaw)) * cos(glm::radians(cPitch));
		fwd.z = sin(glm::radians(cYaw)) * cos(glm::radians(cPitch));
		fwd.y = sin(glm::radians(cPitch));
		fwd = glm::normalize(fwd);
		camera_dir = fwd;
	}

	void glfw_callback_mouse_button(GLFWwindow* window, int button, int action, int mods) {
		if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
			if (!mousecam_on) {
				glfwGetCursorPos(window, &lastX, &lastY);
			}
			mousecam_on = !mousecam_on;
		}

	}
}

// update scene uniforms
namespace
{
	//void update_scene_uniforms(glsl::SceneUniform& aSceneUniforms, glsl::FragSceneUniform& aFragUniforms, std::uint32_t aFramebufferWidth, std::uint32_t aFramebufferHeight)
	void update_scene_uniforms(glsl::SceneUniform& aSceneUniforms, std::uint32_t aFramebufferWidth, std::uint32_t aFramebufferHeight)
	{
		float const aspect = aFramebufferWidth / float(aFramebufferHeight);
		aSceneUniforms.projection = glm::perspectiveRH_ZO(
			lut::Radians(cfg::kCameraFov).value(),
			aspect,
			cfg::kCameraNear,
			cfg::kCameraFar
		);
		aSceneUniforms.projection[1][1] *= -1.f; // mirror Y axis

		aSceneUniforms.camera = glm::lookAt(camera_pos, camera_pos + camera_dir, world_up);

		aSceneUniforms.projCam = aSceneUniforms.projection * aSceneUniforms.camera;

		aSceneUniforms.cameraPosition = glm::vec4(camera_pos, glm::floor( cfg::kLightCount * plightsOn));

		aSceneUniforms.resolution = glm::vec4(windowWidth, windowHeight, 1, 1);
	}
}

namespace
{
	lut::RenderPass create_render_pass(lut::VulkanWindow const& aWindow)
	{
		VkAttachmentDescription attachments[4]{};
		attachments[0].format = cfg::kColorBufferFormat;
		attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
		attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		//attachments[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		attachments[0].finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		attachments[1].format = cfg::kDepthFormat;
		attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
		attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		//attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE; // store depth buffer for deffered
		attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		//attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		attachments[1].finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		attachments[2].format = cfg::kColorBufferFormat;
		attachments[2].samples = VK_SAMPLE_COUNT_1_BIT;
		attachments[2].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		attachments[2].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachments[2].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		//attachments[2].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		attachments[2].finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		// final output attachment
		attachments[3].format = aWindow.swapchainFormat;
		attachments[3].samples = VK_SAMPLE_COUNT_1_BIT;
		attachments[3].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		attachments[3].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachments[3].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		attachments[3].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

		VkAttachmentReference subpassAttachments[2]{};
		subpassAttachments[0].attachment = 0; // this refers to attachments[0]
		subpassAttachments[0].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		//subpassAttachments[0].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		subpassAttachments[1].attachment = 2; // this refers to attachments[2]
		subpassAttachments[1].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		//subpassAttachments[1].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

		// New in section 6
		VkAttachmentReference depthAttachment{};
		depthAttachment.attachment = 1; // this refers to attachments[1]
		depthAttachment.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		//depthAttachment.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		
		// POSTPROC ATTACHMENTS
		VkAttachmentReference postSubpassAttachments[1]{};
		postSubpassAttachments[0].attachment = 3; // this refers to attachments[3]
		postSubpassAttachments[0].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

		VkAttachmentReference postPassInputAttachments[3]{};
		postPassInputAttachments[0].attachment = 0;
		postPassInputAttachments[0].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		postPassInputAttachments[1].attachment = 1;
		postPassInputAttachments[1].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		postPassInputAttachments[2].attachment = 2;
		postPassInputAttachments[2].layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;



		VkSubpassDescription subpasses[2]{};
		subpasses[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpasses[0].colorAttachmentCount = 2; // changed
		subpasses[0].pColorAttachments = subpassAttachments;
		subpasses[0].pDepthStencilAttachment = &depthAttachment;

		subpasses[1].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpasses[1].colorAttachmentCount = 1; // changed
		subpasses[1].pColorAttachments = postSubpassAttachments;

		subpasses[1].inputAttachmentCount = 3;
		subpasses[1].pInputAttachments = postPassInputAttachments;

		// add dependancy between passes
		// based on writing to attachment
		VkSubpassDependency dependency[1]{};
		dependency[0].srcSubpass = 0;
		dependency[0].dstSubpass = 1;
		dependency[0].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		dependency[0].dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
		dependency[0].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		dependency[0].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		//dependency[1].srcSubpass = 0;
		//dependency[1].dstSubpass = 0;
		//dependency[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		//dependency[1].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		//dependency[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		//dependency[1].dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;



		VkRenderPassCreateInfo passInfo{};
		passInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		passInfo.attachmentCount = 4; // changed
		passInfo.pAttachments = attachments;
		passInfo.subpassCount = 2; // changed
		passInfo.pSubpasses = subpasses;
		passInfo.dependencyCount = 1;
		passInfo.pDependencies = dependency; 

		VkRenderPass rpass = VK_NULL_HANDLE;
		if (auto const res = vkCreateRenderPass(aWindow.device, &passInfo, nullptr, &rpass); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create render pass\n"
				"vkCreateRenderPass() returned % s", lut::to_string(res).c_str());
		}


		return lut::RenderPass(aWindow.device, rpass);
	}


	lut::Pipeline create_prebuffer_pipeline(lut::VulkanWindow const& aWindow, VkRenderPass aRenderPass, VkPipelineLayout aPipelineLayout)
	{
		// Load shader modules
		// For this example, we only use the	ertex and fragment shaders. Other
		// shader stages (geometry, tessellation) aren’t used here, and as such we
		// omit them.
		//lut::ShaderModule vert = lut::load_shader_module(aWindow, cfg::kVertShaderPath);
		//lut::ShaderModule frag = lut::load_shader_module(aWindow, cfg::kFragShaderPath);
		lut::ShaderModule vert = lut::load_shader_module(aWindow, cfg::kPreVertPath);
		lut::ShaderModule frag = lut::load_shader_module(aWindow, cfg::kPreFragPath);


		// Deﬁne shader stages in the pipeline
		VkPipelineShaderStageCreateInfo stages[2]{};
		stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
		stages[0].module = vert.handle;
		stages[0].pName = "main";

		stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		stages[1].module = frag.handle;
		stages[1].pName = "main";

		// vertex input attributes
		VkVertexInputBindingDescription	vertexInputs[4]{}; // how data is read from buffers
		// position buffer
		vertexInputs[0].binding = 0;
		vertexInputs[0].stride = sizeof(float) * 3; // = 3 floats, aka vec3 for 3D
		vertexInputs[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		// texcoord buffer
		vertexInputs[1].binding = 1;
		vertexInputs[1].stride = sizeof(float) * 2; // = 2 floats, aka vec2
		vertexInputs[1].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		// tex ID buffer
		vertexInputs[2].binding = 2;
		vertexInputs[2].stride = sizeof(int32_t);
		vertexInputs[2].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		// normals buffer
		vertexInputs[3].binding = 3;
		vertexInputs[3].stride = sizeof(float) * 3;
		vertexInputs[3].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		// set up mapping between buffers and vertex shader
		VkVertexInputAttributeDescription vertexAttributes[4]{}; // how data is mapped to vertex shader's inputs
		vertexAttributes[0].binding = 0; // must match binding above
		vertexAttributes[0].location = 0; // must match shader
		vertexAttributes[0].format = VK_FORMAT_R32G32B32_SFLOAT; // RGB, 32f each - 3 vals for 3D
		vertexAttributes[0].offset = 0;
		// texcoord
		vertexAttributes[1].binding = 1; // must match binding above
		vertexAttributes[1].location = 1; // must match shader
		vertexAttributes[1].format = VK_FORMAT_R32G32_SFLOAT;
		vertexAttributes[1].offset = 0;
		// texID
		vertexAttributes[2].binding = 2; // must match binding above
		vertexAttributes[2].location = 2; // must match shader
		vertexAttributes[2].format = VK_FORMAT_R32_SINT; // S is for signed, U is for unsigned // we want signed to allow for -1
		vertexAttributes[2].offset = 0;
		// normals
		vertexAttributes[3].binding = 3; // must match binding above
		vertexAttributes[3].location = 3; // must match shader
		vertexAttributes[3].format = VK_FORMAT_R32G32B32_SFLOAT;
		vertexAttributes[3].offset = 0;

		VkPipelineVertexInputStateCreateInfo inputInfo{};
		inputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		// adding these for the add'l vertex attributes (ex 1.4)
		inputInfo.vertexBindingDescriptionCount = 4; // number of vertexInputs above (buffers)
		inputInfo.pVertexBindingDescriptions = vertexInputs;
		inputInfo.vertexAttributeDescriptionCount = 4; // number of vertexAttributes above (mappings)
		inputInfo.pVertexAttributeDescriptions = vertexAttributes;

		// Deﬁne which primitive (point, line, triangle, ...) the input is
		// assembled into for rasterization.
		VkPipelineInputAssemblyStateCreateInfo assemblyInfo{};
		assemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		assemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		assemblyInfo.primitiveRestartEnable = VK_FALSE;

		// depth buffer info struct
		// section 6
		VkPipelineDepthStencilStateCreateInfo depthInfo{};
		depthInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
		depthInfo.depthTestEnable = VK_TRUE;
		depthInfo.depthWriteEnable = VK_TRUE;
		depthInfo.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
		depthInfo.minDepthBounds = 0.f;
		depthInfo.maxDepthBounds = 1.f;

		// Deﬁne viewport and scissor regions
		VkViewport viewport{};
		viewport.x = 0.f;
		viewport.y = 0.f;
		viewport.width = float(aWindow.swapchainExtent.width);
		viewport.height = float(aWindow.swapchainExtent.height);
		viewport.minDepth = 0.f;
		viewport.maxDepth = 1.f;

		VkRect2D scissor{};
		scissor.offset = VkOffset2D{ 0, 0 };
		scissor.extent = aWindow.swapchainExtent;

		VkPipelineViewportStateCreateInfo	viewportInfo{};
		viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportInfo.viewportCount = 1;
		viewportInfo.pViewports = &viewport;
		viewportInfo.scissorCount = 1;
		viewportInfo.pScissors = &scissor;

		// Deﬁne rasterization options
		VkPipelineRasterizationStateCreateInfo rasterInfo{};
		rasterInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterInfo.depthClampEnable = VK_FALSE;
		rasterInfo.rasterizerDiscardEnable = VK_FALSE;
		rasterInfo.polygonMode = VK_POLYGON_MODE_FILL;
		rasterInfo.cullMode = VK_CULL_MODE_BACK_BIT;
		rasterInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rasterInfo.depthBiasEnable = VK_FALSE;
		rasterInfo.lineWidth = 1.f; // required.

		// Deﬁne multisampling state
		VkPipelineMultisampleStateCreateInfo samplingInfo{};
		samplingInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		samplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

		// Deﬁne blend state
		// We deﬁne one blend state per color attachment - this example uses a
		// single color attachment, so we only need one. Right now, we don’t do any
		// blending, so we can ignore most of the members.
		VkPipelineColorBlendAttachmentState blendStates[2]{}; // changed for second color attachment
		blendStates[0].blendEnable = VK_FALSE;
		blendStates[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT
			| VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		blendStates[1].blendEnable = VK_FALSE;
		blendStates[1].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT
			| VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

		VkPipelineColorBlendStateCreateInfo blendInfo{};
		blendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		blendInfo.logicOpEnable = VK_FALSE;
		blendInfo.attachmentCount = 2; // changed for second color attachment
		blendInfo.pAttachments = blendStates;

		// Create pipeline
		VkGraphicsPipelineCreateInfo pipeInfo{};
		pipeInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipeInfo.stageCount = 2;	// vertex + fragment stages
		pipeInfo.pStages = stages;

		pipeInfo.pVertexInputState = &inputInfo;
		pipeInfo.pInputAssemblyState = &assemblyInfo;
		pipeInfo.pTessellationState = nullptr; // no tessellation
		pipeInfo.pViewportState = &viewportInfo;
		pipeInfo.pRasterizationState = &rasterInfo;
		pipeInfo.pMultisampleState = &samplingInfo;
		pipeInfo.pDepthStencilState = &depthInfo; // new in section 6
		pipeInfo.pColorBlendState = &blendInfo;
		pipeInfo.pDynamicState = nullptr; // no dynamic states
		pipeInfo.layout = aPipelineLayout;
		pipeInfo.renderPass = aRenderPass;
		pipeInfo.subpass = 0; // ﬁrst subpass of aRenderPass


		VkPipeline pipe = VK_NULL_HANDLE;
		if (auto const res = vkCreateGraphicsPipelines(aWindow.device, VK_NULL_HANDLE, 1,
			&pipeInfo, nullptr, &pipe); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create graphics pipeline\n"
				"vkCreateGraphicsPipelines() returned % s", lut::to_string(res).c_str()
			);
		}
		return lut::Pipeline(aWindow.device, pipe);
	}

	// non-triangle pipeline functions
	lut::PipelineLayout create_prebuffer_pipeline_layout(lut::VulkanContext const& aContext, 
		VkDescriptorSetLayout aSceneLayout, // VkDescriptorSetLayout aModelLayout,
		VkDescriptorSetLayout aMaterialsLayout,
		VkDescriptorSetLayout aTexLayout
	)
	{
		VkDescriptorSetLayout layouts[] = {
			// Order must match the set = N in the shaders
			aSceneLayout,			// set 0
			//aModelLayout			// set 1
			aMaterialsLayout,		// set 1
			aTexLayout				// set 2
		};

		VkPipelineLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		layoutInfo.setLayoutCount = sizeof(layouts) / sizeof(layouts[0]); // updated!
		layoutInfo.pSetLayouts = layouts; // updated!
		layoutInfo.pushConstantRangeCount = 0;
		layoutInfo.pPushConstantRanges = nullptr;

		VkPipelineLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreatePipelineLayout(aContext.device, &layoutInfo, nullptr, &layout);
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create pipeline layout\n"
				"vkCreatePipelineLayout() returned % s", lut::to_string(res).c_str());
		}

		return lut::PipelineLayout(aContext.device, layout);
	}

	lut::Pipeline create_postbuffer_pipeline(lut::VulkanWindow const& aWindow, VkRenderPass aRenderPass, VkPipelineLayout aPipelineLayout)
	{
		// Load shader modules
		// For this example, we only use the	ertex and fragment shaders. Other
		// shader stages (geometry, tessellation) aren’t used here, and as such we
		// omit them.
		//lut::ShaderModule vert = lut::load_shader_module(aWindow, cfg::kVertShaderPath);
		//lut::ShaderModule frag = lut::load_shader_module(aWindow, cfg::kFragShaderPath);
		lut::ShaderModule vert = lut::load_shader_module(aWindow, cfg::kPostVertPath);
		lut::ShaderModule frag = lut::load_shader_module(aWindow, cfg::kPostFragPath);


		// Deﬁne shader stages in the pipeline
		VkPipelineShaderStageCreateInfo stages[2]{};
		stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
		stages[0].module = vert.handle;
		stages[0].pName = "main";

		stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		stages[1].module = frag.handle;
		stages[1].pName = "main";

		VkPipelineVertexInputStateCreateInfo inputInfo{};
		inputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		
		// Deﬁne which primitive (point, line, triangle, ...) the input is
		// assembled into for rasterization.
		VkPipelineInputAssemblyStateCreateInfo assemblyInfo{};
		assemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		assemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP; // changed to strip from list
		assemblyInfo.primitiveRestartEnable = VK_FALSE;


		// Deﬁne viewport and scissor regions
		VkViewport viewport{};
		viewport.x = 0.f;
		viewport.y = 0.f;
		viewport.width = float(aWindow.swapchainExtent.width);
		viewport.height = float(aWindow.swapchainExtent.height);
		viewport.minDepth = 0.f;
		viewport.maxDepth = 1.f;

		VkRect2D scissor{};
		scissor.offset = VkOffset2D{ 0, 0 };
		scissor.extent = aWindow.swapchainExtent;

		VkPipelineViewportStateCreateInfo	viewportInfo{};
		viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportInfo.viewportCount = 1;
		viewportInfo.pViewports = &viewport;
		viewportInfo.scissorCount = 1;
		viewportInfo.pScissors = &scissor;

		// Deﬁne rasterization options
		VkPipelineRasterizationStateCreateInfo rasterInfo{};
		rasterInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterInfo.depthClampEnable = VK_FALSE;
		rasterInfo.rasterizerDiscardEnable = VK_FALSE;
		rasterInfo.polygonMode = VK_POLYGON_MODE_FILL;
		rasterInfo.cullMode = VK_CULL_MODE_BACK_BIT;
		rasterInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rasterInfo.depthBiasEnable = VK_FALSE;
		rasterInfo.lineWidth = 1.f; // required.

		// Deﬁne multisampling state
		VkPipelineMultisampleStateCreateInfo samplingInfo{};
		samplingInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		samplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

		// Deﬁne blend state
		// We deﬁne one blend state per color attachment - this example uses a
		// single color attachment, so we only need one. Right now, we don’t do any
		// blending, so we can ignore most of the members.
		VkPipelineColorBlendAttachmentState blendStates[1]{}; // changed for second color attachment
		blendStates[0].blendEnable = VK_FALSE;
		blendStates[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT
			| VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;

		VkPipelineColorBlendStateCreateInfo blendInfo{};
		blendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		blendInfo.logicOpEnable = VK_FALSE;
		blendInfo.attachmentCount = 1;
		blendInfo.pAttachments = blendStates;

		// Create pipeline
		VkGraphicsPipelineCreateInfo pipeInfo{};
		pipeInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipeInfo.stageCount = 2;	// vertex + fragment stages
		pipeInfo.pStages = stages;

		pipeInfo.pVertexInputState = &inputInfo;
		pipeInfo.pInputAssemblyState = &assemblyInfo;
		pipeInfo.pTessellationState = nullptr; // no tessellation
		pipeInfo.pViewportState = &viewportInfo;
		pipeInfo.pRasterizationState = &rasterInfo;
		pipeInfo.pMultisampleState = &samplingInfo;
		pipeInfo.pDepthStencilState = nullptr; // no need to deal with depth since its culled in first pass
		pipeInfo.pColorBlendState = &blendInfo;
		pipeInfo.pDynamicState = nullptr; // no dynamic states
		pipeInfo.layout = aPipelineLayout;
		pipeInfo.renderPass = aRenderPass;
		pipeInfo.subpass = 1; // second subpass of aRenderPass


		VkPipeline pipe = VK_NULL_HANDLE;
		if (auto const res = vkCreateGraphicsPipelines(aWindow.device, VK_NULL_HANDLE, 1,
			&pipeInfo, nullptr, &pipe); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create graphics pipeline\n"
				"vkCreateGraphicsPipelines() returned % s", lut::to_string(res).c_str()
			);
		}
		return lut::Pipeline(aWindow.device, pipe);
	}

	// non-triangle pipeline functions
	lut::PipelineLayout create_postbuffer_pipeline_layout(lut::VulkanContext const& aContext,
		VkDescriptorSetLayout aSceneLayout,
		VkDescriptorSetLayout gBufferLayout,
		VkDescriptorSetLayout aLightsLayout
	)
	{
		VkDescriptorSetLayout layouts[] = {
			// Order must match the set = N in the shaders
			aSceneLayout,			// set 0
			gBufferLayout,
			aLightsLayout			// set 2
		};

		VkPipelineLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		layoutInfo.setLayoutCount = sizeof(layouts) / sizeof(layouts[0]); // updated!
		layoutInfo.pSetLayouts = layouts; // updated!
		layoutInfo.pushConstantRangeCount = 0;
		layoutInfo.pPushConstantRanges = nullptr;

		VkPipelineLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreatePipelineLayout(aContext.device, &layoutInfo, nullptr, &layout);
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create pipeline layout\n"
				"vkCreatePipelineLayout() returned % s", lut::to_string(res).c_str());
		}

		return lut::PipelineLayout(aContext.device, layout);
	}

	void create_swapchain_framebuffers(lut::VulkanWindow const& aWindow, VkRenderPass aRenderPass, std::vector<lut::Framebuffer>& aFramebuffers,
		VkImageView aDepthView,
		VkImageView aColorView,
		VkImageView aNormsView
	)
	{
		assert(aFramebuffers.empty());

		for (std::size_t i = 0; i < aWindow.swapViews.size(); ++i)
		{
			VkImageView attachments[4] = {
				aColorView,
				aDepthView,
				aNormsView,
				aWindow.swapViews[i]
			};

			VkFramebufferCreateInfo fbInfo{};
			fbInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
			fbInfo.flags = 0; // normal framebuﬀer
			fbInfo.renderPass = aRenderPass;
			fbInfo.attachmentCount = 4; // CHANGED
			fbInfo.pAttachments = attachments;
			fbInfo.width = aWindow.swapchainExtent.width;
			fbInfo.height = aWindow.swapchainExtent.height;
			fbInfo.layers = 1;

			VkFramebuffer fb = VK_NULL_HANDLE;
			if (auto const res = vkCreateFramebuffer(aWindow.device, &fbInfo, nullptr, &fb); VK_SUCCESS != res)
			{
				throw lut::Error("Unable to create framebuffer for swap chain image %zu\n"
					"vkCreateFramebuffer() returned %s", i, lut::to_string(res).c_str());
			}


			aFramebuffers.emplace_back(lut::Framebuffer(aWindow.device, fb));
		}

		assert(aWindow.swapViews.size() == aFramebuffers.size());
	}

	lut::DescriptorSetLayout create_scene_descriptor_layout(lut::VulkanWindow const& aWindow)
	{
		// Section 3 of ex4 (page 13)
		VkDescriptorSetLayoutBinding bindings[1]{};
		bindings[0].binding = 0; // number must match the index of the corresponding
								 // binding = N declaration in the shader(s)!
		bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		bindings[0].descriptorCount = 1;
		bindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;

		VkDescriptorSetLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = sizeof(bindings) / sizeof(bindings[0]);
		layoutInfo.pBindings = bindings;

		VkDescriptorSetLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreateDescriptorSetLayout(aWindow.device, &layoutInfo, nullptr, &layout); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create descriptor set layout\n"
				"vkCreateDescriptorSetLayout() returned % s", lut::to_string(res).c_str()
			);
		}

		return lut::DescriptorSetLayout(aWindow.device, layout);

	}

	lut::DescriptorSetLayout create_object_descriptor_layout(lut::VulkanWindow const& aWindow)
	{
		VkDescriptorSetLayoutBinding bindings[1]{};
		bindings[0].binding = 0; // this must match the X value in the shader layout (binding = X) 
		bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		bindings[0].descriptorCount = 1;
		bindings[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		VkDescriptorSetLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = sizeof(bindings) / sizeof(bindings[0]);
		layoutInfo.pBindings = bindings;

		VkDescriptorSetLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreateDescriptorSetLayout(aWindow.device, &layoutInfo, nullptr, &layout); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create descriptor set layout\n"
				"vkCreateDescriptorSetLayout() returned % s", lut::to_string(res).c_str());
		}

		return lut::DescriptorSetLayout(aWindow.device, layout);
	}

	lut::DescriptorSetLayout create_material_descriptor_layout(lut::VulkanWindow const& aWindow, uint32_t maxDescriptorSets)
	{
		VkDescriptorSetLayoutBinding bindings[1]{};
		// frag scene info, materials, lights

		// frag scene info binding : 0 
		bindings[0].binding = 0; // this must match the X value in the shader layout (binding = X) 
		bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		bindings[0].descriptorCount = maxDescriptorSets;
		bindings[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		// need one per binding
		VkDescriptorBindingFlags bindingFlags[1]{ VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT_EXT };

		VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlagsInfo{};
		bindingFlagsInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
		bindingFlagsInfo.bindingCount = 1;
		bindingFlagsInfo.pBindingFlags = &bindingFlags[0];

		VkDescriptorSetLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = sizeof(bindings) / sizeof(bindings[0]);
		layoutInfo.pBindings = bindings;
		layoutInfo.pNext = &bindingFlagsInfo;

		VkDescriptorSetLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreateDescriptorSetLayout(aWindow.device, &layoutInfo, nullptr, &layout); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create descriptor set layout\n"
				"vkCreateDescriptorSetLayout() returned % s", lut::to_string(res).c_str());
		}

		return lut::DescriptorSetLayout(aWindow.device, layout);
	}

	lut::DescriptorSetLayout create_lights_descriptor_layout(lut::VulkanWindow const& aWindow, uint32_t maxDescriptorSets)
	{
		VkDescriptorSetLayoutBinding bindings[1]{};
		
		bindings[0].binding = 0; // this must match the X value in the shader layout (binding = X) 
		bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		bindings[0].descriptorCount = maxDescriptorSets;
		bindings[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		// need one per binding
		VkDescriptorBindingFlags bindingFlags[1]{ VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT_EXT };

		VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlagsInfo{};
		bindingFlagsInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
		bindingFlagsInfo.bindingCount = 1;
		bindingFlagsInfo.pBindingFlags = &bindingFlags[0];


		VkDescriptorSetLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = sizeof(bindings) / sizeof(bindings[0]);
		layoutInfo.pBindings = bindings;
		layoutInfo.pNext = &bindingFlagsInfo;

		VkDescriptorSetLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreateDescriptorSetLayout(aWindow.device, &layoutInfo, nullptr, &layout); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create descriptor set layout\n"
				"vkCreateDescriptorSetLayout() returned % s", lut::to_string(res).c_str());
		}

		return lut::DescriptorSetLayout(aWindow.device, layout);
	}

	lut::DescriptorSetLayout create_texture_descriptor_layout(lut::VulkanWindow const& aWindow, uint32_t maxDescriptorSets)
	{
		VkDescriptorSetLayoutBinding bindings[1]{};
		bindings[0].binding = 0; // this must match the X value in the shader layout (binding = X) 
		bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		bindings[0].descriptorCount = maxDescriptorSets; // get from VkPhysicalDeviceProperties.limits.maxDescriptorSetSamplers
		//bindings[0].descriptorCount = aModel.materials.size(); // this allows us to have 1 texture per material 
		bindings[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		VkDescriptorBindingFlags bindingFlags[1]{ VK_DESCRIPTOR_BINDING_VARIABLE_DESCRIPTOR_COUNT_BIT_EXT };

		VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlagsInfo{};
		bindingFlagsInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
		bindingFlagsInfo.bindingCount = 1;
		bindingFlagsInfo.pBindingFlags = &bindingFlags[0];


		VkDescriptorSetLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = sizeof(bindings) / sizeof(bindings[0]);
		layoutInfo.pBindings = bindings;
		layoutInfo.pNext = &bindingFlagsInfo;

		VkDescriptorSetLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreateDescriptorSetLayout(aWindow.device, &layoutInfo, nullptr, &layout); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create descriptor set layout\n"
				"vkCreateDescriptorSetLayout() returned % s", lut::to_string(res).c_str());
		}

		return lut::DescriptorSetLayout(aWindow.device, layout);
	}

	lut::DescriptorSetLayout create_gbuffer_descriptor_layout(lut::VulkanWindow const& aWindow)
	{
		VkDescriptorSetLayoutBinding bindings[3]{};
		bindings[0].binding = 0; // this must match the X value in the shader layout (binding = X) 
		//bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		bindings[0].descriptorCount = 1;
		bindings[0].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		bindings[1].binding = 1; // this must match the X value in the shader layout (binding = X) 
		//bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		bindings[1].descriptorCount = 1;
		bindings[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

		bindings[2].binding = 2; // this must match the X value in the shader layout (binding = X) 
		//bindings[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		bindings[2].descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
		bindings[2].descriptorCount = 1;
		bindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;



		VkDescriptorSetLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		layoutInfo.bindingCount = sizeof(bindings) / sizeof(bindings[0]);
		layoutInfo.pBindings = bindings;

		VkDescriptorSetLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreateDescriptorSetLayout(aWindow.device, &layoutInfo, nullptr, &layout); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create descriptor set layout\n"
				"vkCreateDescriptorSetLayout() returned % s", lut::to_string(res).c_str());
		}

		return lut::DescriptorSetLayout(aWindow.device, layout);
	}


	void record_commands(VkCommandBuffer aCmdBuff, VkRenderPass aRenderPass, VkFramebuffer aFramebuffer, 
		VkPipeline aPrePipe,
		VkPipeline aPostPipe,
		VkExtent2D const& aImageExtent,
		BufferMesh& aBuffers,
		//BufferMesh& bBuffers,
		VkBuffer aSceneUBO, glsl::SceneUniform const& aSceneUniform,
		std::vector<lut::Buffer> const&lightUBOvector,
		std::vector<lut::Image*> const& gBufferImages,
		VkPipelineLayout aPrePipelineLayout, 
		VkPipelineLayout aPostPipelineLayout,
		VkDescriptorSet aSceneDescriptors,
		//VkDescriptorSet aModelDescriptors
		//VkDescriptorSet aFragSceneDescriptors,
		VkDescriptorSet aMaterialDescriptors,
		VkDescriptorSet aLightsDescriptors,
		VkDescriptorSet aTexDescriptors,
		VkDescriptorSet gBufferDescriptors
	) // end of params for record commands
	{
		// Begin recording commands
		VkCommandBufferBeginInfo begInfo{};
		begInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		begInfo.pInheritanceInfo = nullptr;
		if (auto const res = vkBeginCommandBuffer(aCmdBuff, &begInfo); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to begin recording command buffer\n"
				"vkBeginCommandBuffer() returned % s", lut::to_string(res).c_str());
		}

		// Upload scene uniforms
		// section 3 p19
		lut::buffer_barrier(aCmdBuff,
			aSceneUBO,
			VK_ACCESS_UNIFORM_READ_BIT,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT | VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, // changed to accomodate frag shader too 
			VK_PIPELINE_STAGE_TRANSFER_BIT
		); // ensure vkCmdUpdateBuffer only takes place after previous vertex shader stages are done
		// don't want uniforms changing mid vertex shader stage

		// update scene uniforms
		vkCmdUpdateBuffer(aCmdBuff, aSceneUBO, 0, sizeof(glsl::SceneUniform), &aSceneUniform);

		lut::buffer_barrier(aCmdBuff,
			aSceneUBO,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			VK_ACCESS_UNIFORM_READ_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT | VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT // changed to allow use in frag shader in additon to vert shader
		); // ensures copy is complete before allowing buffer to be read
		// don't want half-copied data being used

		// update light positions for animation
		// calc time for animation rotation matrix, only increment uTime while animation should be playing
		// this way we don't snap to fixed positon 
		if (lanimated) {
			double timeStep = glfwGetTime() - lastTime;
			uTime += timeStep;
		}
		lastTime = glfwGetTime();

		glm::mat4 lTransform = glm::rotate((float)glm::radians(10.1 * uTime), world_up);
		for (int i = 0; i < cfg::kLightCount; i++)
			update_light_buffer(lTransform, lightUBOvector[i].buffer, i, aCmdBuff);

		// end updating uniforms

		// create image barrier to be used betwen subpasses
		VkImageMemoryBarrier gBufferBarrier{};
		gBufferBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
		gBufferBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		gBufferBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
		gBufferBarrier.subresourceRange.baseArrayLayer = 0;
		gBufferBarrier.subresourceRange.baseMipLevel = 0;
		gBufferBarrier.subresourceRange.layerCount = 1;
		gBufferBarrier.subresourceRange.levelCount = 1;
		gBufferBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		gBufferBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		gBufferBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		//start with depth buffer, so these will change
		gBufferBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT; 
		gBufferBarrier.oldLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;


		// Begin render pass
		VkClearValue clearValues[4]{};
		// ensure we clear color buffer to black
		clearValues[0].color.float32[0] = 0.0f;
		clearValues[0].color.float32[1] = 0.0f;
		clearValues[0].color.float32[2] = 0.0f;
		clearValues[0].color.float32[3] = 0.0f;

		clearValues[1].depthStencil.depth = 1.f; // clear depth buffer

		// ensure we clear color buffer to black
		clearValues[2].color.float32[0] = 0.0f; 
		clearValues[2].color.float32[1] = 0.0f;
		clearValues[2].color.float32[2] = 0.0f;
		clearValues[2].color.float32[3] = 0.0f; 

		clearValues[3].color.float32[0] = 0.1f; // Clear to a dark gray background.
		clearValues[3].color.float32[1] = 0.1f; // If we were debugging, this would potentially
		clearValues[3].color.float32[2] = 0.1f; // help us see whether the render pass took
		clearValues[3].color.float32[3] = 1.f;  // place, even if nothing else was drawn.

		VkRenderPassBeginInfo passInfo{};
		passInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		passInfo.renderPass = aRenderPass;
		passInfo.framebuffer = aFramebuffer;
		passInfo.renderArea.offset = VkOffset2D{ 0, 0 };
		passInfo.renderArea.extent = aImageExtent;
		passInfo.clearValueCount = 4;
		passInfo.pClearValues = clearValues;

		vkCmdBeginRenderPass(aCmdBuff, &passInfo, VK_SUBPASS_CONTENTS_INLINE);

		// Begin drawing with our graphics pipeline
		vkCmdBindPipeline(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aPrePipe);

		// must bind descriptor sets before draw call
		vkCmdBindDescriptorSets(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aPrePipelineLayout, 0, 1, &aSceneDescriptors, 0, nullptr);
		// must bind descriptor sets before draw call
		vkCmdBindDescriptorSets(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aPrePipelineLayout, 1, 1, &aMaterialDescriptors, 0, nullptr);
		vkCmdBindDescriptorSets(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aPrePipelineLayout, 2, 1, &aTexDescriptors, 0, nullptr);
		
		// Bind	vertex input

		// get buffers from buffer object
		VkBuffer abuffers[4] = {
			aBuffers.positions.buffer,
			aBuffers.texcoords.buffer,
			aBuffers.texids.buffer,
			aBuffers.normals.buffer
		};
		VkDeviceSize aoffsets[4]{};

		vkCmdBindVertexBuffers(aCmdBuff, 0, 4, abuffers, aoffsets);
		// Draw	vertices
		vkCmdDraw(aCmdBuff, aBuffers.vertexCount, 1, 0, 0);

		// use image barriers to change format
		//for (lut::Image* i : gBufferImages) {
		//	gBufferBarrier.image = i->image;
		//	vkCmdPipelineBarrier(aCmdBuff,
		//		VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0,
		//		0, nullptr,
		//		0, nullptr,
		//		1, &gBufferBarrier);
		//	// all uses of gBufferBarrier after the first are on color attachments
		//	// this is a little lazy but oh well
		//	gBufferBarrier.oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		//	gBufferBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		//}

		// Begin second pass
		vkCmdNextSubpass(aCmdBuff, VK_SUBPASS_CONTENTS_INLINE);
		

		vkCmdBindPipeline(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aPostPipe);

		// must bind descriptor sets before draw call
		vkCmdBindDescriptorSets(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aPostPipelineLayout, 0, 1, &aSceneDescriptors, 0, nullptr);
		vkCmdBindDescriptorSets(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aPostPipelineLayout, 1, 1, &gBufferDescriptors, 0, nullptr);
		vkCmdBindDescriptorSets(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aPostPipelineLayout, 2, 1, &aLightsDescriptors, 0, nullptr);

		vkCmdDraw(aCmdBuff, 4, 1, 0, 0);

		// End the render pass
		vkCmdEndRenderPass(aCmdBuff);



		// End command recording
		if (auto const res = vkEndCommandBuffer(aCmdBuff); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to end recording command buffer\n"
				"vkEndCommandBuffer() returned % s", lut::to_string(res).c_str());
		}

	}

	void submit_commands(lut::VulkanWindow const& aWindow, VkCommandBuffer aCmdBuff, VkFence aFence, VkSemaphore aWaitSemaphore, VkSemaphore aSignalSemaphore)
	{
		VkPipelineStageFlags waitPipelineStages =
			VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

		VkSubmitInfo submitInfo{};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &aCmdBuff;

		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = &aWaitSemaphore;
		submitInfo.pWaitDstStageMask = &waitPipelineStages;

		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = &aSignalSemaphore;

		if (auto const res = vkQueueSubmit(aWindow.graphicsQueue, 1, &submitInfo, aFence); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to submit command buffer to queue\n"
				"vkQueueSubmit() returned % s", lut::to_string(res).c_str());
		}
	}

	void present_results(VkQueue aPresentQueue, VkSwapchainKHR aSwapchain, std::uint32_t aImageIndex, VkSemaphore aRenderFinished, bool& aNeedToRecreateSwapchain)
	{
		// Present the results
		VkPresentInfoKHR presentInfo{};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = &aRenderFinished;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &aSwapchain;
		presentInfo.pImageIndices = &aImageIndex;
		presentInfo.pResults = nullptr;

		auto const presentRes = vkQueuePresentKHR(aPresentQueue, &presentInfo);

		if (VK_SUBOPTIMAL_KHR == presentRes || VK_ERROR_OUT_OF_DATE_KHR == presentRes)
		{
			aNeedToRecreateSwapchain = true;
		}
		else if (VK_SUCCESS != presentRes)
		{
			throw lut::Error("Unable present swapchain image %u\n"
				"vkQueuePresentKHR() returned % s", aImageIndex, lut::to_string(presentRes).c_str());
		}
	}
}

namespace
{
	std::tuple<lut::Image, lut::ImageView> create_depth_buffer(lut::VulkanWindow const& aWindow, lut::Allocator const& aAllocator)
	{
		VkImageCreateInfo imageInfo{};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.format = cfg::kDepthFormat;
		imageInfo.extent.width = aWindow.swapchainExtent.width;
		imageInfo.extent.height = aWindow.swapchainExtent.height;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageInfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT 
			| VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT
			| VK_IMAGE_USAGE_SAMPLED_BIT;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		VmaAllocationCreateInfo allocInfo{};
		allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;


		VkImage image = VK_NULL_HANDLE;
		VmaAllocation allocation = VK_NULL_HANDLE;

		if (auto const res = vmaCreateImage(aAllocator.allocator, &imageInfo, &allocInfo, &image, &allocation, nullptr);
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to allocate depth buffer image.\n"
				"vmaCreateImage() returned %s", lut::to_string(res).c_str());
		}

		lut::Image depthImage(aAllocator.allocator, image, allocation);

		// Create the image view
		VkImageViewCreateInfo viewInfo{};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = depthImage.image;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = cfg::kDepthFormat;
		viewInfo.components = VkComponentMapping{};
		viewInfo.subresourceRange = VkImageSubresourceRange{
			VK_IMAGE_ASPECT_DEPTH_BIT,
			0, 1,
			0, 1
		};

		VkImageView	view = VK_NULL_HANDLE;
		if (auto const res = vkCreateImageView(aWindow.device, &viewInfo, nullptr, &view);
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create image view\n"
				"vkCreateImageView() returned %s", lut::to_string(res).c_str());
		}


		return { std::move(depthImage), lut::ImageView(aWindow.device, view) };
	}

	std::tuple<lut::Image, lut::ImageView> create_intermediate_buffer(lut::VulkanWindow const& aWindow, lut::Allocator const& aAllocator)
	{
		VkImageCreateInfo imageInfo{};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.format = cfg::kColorBufferFormat; // changed from depth version
		imageInfo.extent.width = aWindow.swapchainExtent.width;
		imageInfo.extent.height = aWindow.swapchainExtent.height;
		imageInfo.extent.depth = 1;
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT 
			| VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT
			| VK_IMAGE_USAGE_SAMPLED_BIT; // changed from depth version
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		VmaAllocationCreateInfo allocInfo{};
		allocInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;


		VkImage image = VK_NULL_HANDLE;
		VmaAllocation allocation = VK_NULL_HANDLE;

		if (auto const res = vmaCreateImage(aAllocator.allocator, &imageInfo, &allocInfo, &image, &allocation, nullptr);
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to allocate color buffer image.\n"
				"vmaCreateImage() returned %s", lut::to_string(res).c_str());
		}

		lut::Image bufferImage(aAllocator.allocator, image, allocation);

		// Create the image view
		VkImageViewCreateInfo viewInfo{};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = bufferImage.image;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = cfg::kColorBufferFormat;
		viewInfo.components = VkComponentMapping{};
		viewInfo.subresourceRange = VkImageSubresourceRange{
			VK_IMAGE_ASPECT_COLOR_BIT,
			0, 1,
			0, 1
		};

		VkImageView	view = VK_NULL_HANDLE;
		if (auto const res = vkCreateImageView(aWindow.device, &viewInfo, nullptr, &view);
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create image view\n"
				"vkCreateImageView() returned %s", lut::to_string(res).c_str());
		}


		return { std::move(bufferImage), lut::ImageView(aWindow.device, view) };
	}
}

// loading buffers
namespace
{
	//void load_blinn_buffer(VkBuffer gpuBuffer, MaterialInfo mat, VkCommandBuffer const& aCmdBuff)
	//{
	//	glsl::MaterialUniform matUniform{};
	//	matUniform.diffuse = glm::vec4(mat.diffuse, 1.);
	//	matUniform.specular = glm::vec4(mat.specular, 1.);
	//	matUniform.emissive = glm::vec4(mat.emissive, 1.);
	//	matUniform.shininess = mat.shininess;
	//
	//	lut::buffer_barrier(aCmdBuff,
	//		gpuBuffer,
	//		VK_ACCESS_UNIFORM_READ_BIT,
	//		VK_ACCESS_TRANSFER_WRITE_BIT,
	//		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, // note only vertex shader bit here
	//		VK_PIPELINE_STAGE_TRANSFER_BIT
	//	); // ensure vkCmdUpdateBuffer only takes place after previous vertex shader stages are done
	//	// don't want uniforms changing mid vertex shader stage
	//
	//	vkCmdUpdateBuffer(aCmdBuff, gpuBuffer, 0, sizeof(glsl::MaterialUniform), &matUniform);
	//
	//	lut::buffer_barrier(aCmdBuff,
	//		gpuBuffer,
	//		VK_ACCESS_TRANSFER_WRITE_BIT,
	//		VK_ACCESS_UNIFORM_READ_BIT,
	//		VK_PIPELINE_STAGE_TRANSFER_BIT,
	//		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT // note only vertex shader bit here
	//	); // ensures copy is complete before allowing buffer to be read
	//	// don't want half-copied data being used
	//}

	void load_PBR_buffer(VkBuffer gpuBuffer, MaterialInfo mat, VkCommandBuffer const& aCmdBuff)
	{
		glsl::MaterialUniform matUniform{};
		matUniform.albedo = glm::vec4(mat.albedo, 1.);
		matUniform.emissive = glm::vec4(mat.emissive, 1.);
		matUniform.shininess = mat.shininess;
		matUniform.metalness = mat.metalness;
	
		lut::buffer_barrier(aCmdBuff,
			gpuBuffer,
			VK_ACCESS_UNIFORM_READ_BIT,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, // note only vertex shader bit here
			VK_PIPELINE_STAGE_TRANSFER_BIT
		); // ensure vkCmdUpdateBuffer only takes place after previous vertex shader stages are done
		// don't want uniforms changing mid vertex shader stage
	
		vkCmdUpdateBuffer(aCmdBuff, gpuBuffer, 0, sizeof(glsl::MaterialUniform), &matUniform);
	
		lut::buffer_barrier(aCmdBuff,
			gpuBuffer,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			VK_ACCESS_UNIFORM_READ_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT // note only vertex shader bit here
		); // ensures copy is complete before allowing buffer to be read
		// don't want half-copied data being used
	}
	
	//void load_both_buffer(VkBuffer gpuBuffer, MaterialInfo mat, VkCommandBuffer const& aCmdBuff)
	//{
	//	glsl::MaterialUniform matUniform{};
	//	matUniform.albedo = glm::vec4(mat.albedo, 1.);
	//	matUniform.emissive = glm::vec4(mat.emissive, 1.);
	//	matUniform.shininess = mat.shininess;
	//	matUniform.metalness = mat.metalness;
	//
	//	lut::buffer_barrier(aCmdBuff,
	//		gpuBuffer,
	//		VK_ACCESS_UNIFORM_READ_BIT,
	//		VK_ACCESS_TRANSFER_WRITE_BIT,
	//		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, // note only vertex shader bit here
	//		VK_PIPELINE_STAGE_TRANSFER_BIT
	//	); // ensure vkCmdUpdateBuffer only takes place after previous vertex shader stages are done
	//	// don't want uniforms changing mid vertex shader stage
	//
	//	vkCmdUpdateBuffer(aCmdBuff, gpuBuffer, 0, sizeof(glsl::MaterialUniform), &matUniform);
	//
	//	lut::buffer_barrier(aCmdBuff,
	//		gpuBuffer,
	//		VK_ACCESS_TRANSFER_WRITE_BIT,
	//		VK_ACCESS_UNIFORM_READ_BIT,
	//		VK_PIPELINE_STAGE_TRANSFER_BIT,
	//		VK_PIPELINE_STAGE_VERTEX_SHADER_BIT // note only vertex shader bit here
	//	); // ensures copy is complete before allowing buffer to be read
	//	// don't want half-copied data being used
	//}

	void load_light_buffer(VkBuffer gpuBuffer, int lightIndex, VkCommandBuffer const& aCmdBuff)
	{
		glsl::LightingUniform lightUniform{};
		lightUniform.lightPos = glm::vec4(cfg::kLightPositions[lightIndex], 1.);
		lightUniform.lightColor = cfg::kLightColors[lightIndex];

		lut::buffer_barrier(aCmdBuff,
			gpuBuffer,
			VK_ACCESS_UNIFORM_READ_BIT,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, // note only vertex shader bit here
			VK_PIPELINE_STAGE_TRANSFER_BIT
		); // ensure vkCmdUpdateBuffer only takes place after previous vertex shader stages are done
		// don't want uniforms changing mid vertex shader stage

		vkCmdUpdateBuffer(aCmdBuff, gpuBuffer, 0, sizeof(glsl::LightingUniform), &lightUniform);

		lut::buffer_barrier(aCmdBuff,
			gpuBuffer,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			VK_ACCESS_UNIFORM_READ_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT // note only vertex shader bit here
		); // ensures copy is complete before allowing buffer to be read
		// don't want half-copied data being used
	}

	void update_light_buffer(glm::mat4 lTransformMat, VkBuffer gpuBuffer, int lightIndex, VkCommandBuffer const& aCmdBuff)
	{
		glsl::LightingUniform lightUniform{};
		lightUniform.lightPos = lTransformMat * glm::vec4(cfg::kLightPositions[lightIndex], 1.);
		lightUniform.lightColor = cfg::kLightColors[lightIndex];

		lut::buffer_barrier(aCmdBuff,
			gpuBuffer,
			VK_ACCESS_UNIFORM_READ_BIT,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, // note only vertex shader bit here
			VK_PIPELINE_STAGE_TRANSFER_BIT
		); // ensure vkCmdUpdateBuffer only takes place after previous vertex shader stages are done
		// don't want uniforms changing mid vertex shader stage

		vkCmdUpdateBuffer(aCmdBuff, gpuBuffer, 0, sizeof(glsl::LightingUniform), &lightUniform);

		lut::buffer_barrier(aCmdBuff,
			gpuBuffer,
			VK_ACCESS_TRANSFER_WRITE_BIT,
			VK_ACCESS_UNIFORM_READ_BIT,
			VK_PIPELINE_STAGE_TRANSFER_BIT,
			VK_PIPELINE_STAGE_VERTEX_SHADER_BIT // note only vertex shader bit here
		); // ensures copy is complete before allowing buffer to be read
		// don't want half-copied data being used
	}

}


//EOF vim:syntax=cpp:foldmethod=marker:ts=4:noexpandtab: 
