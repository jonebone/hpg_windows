﻿#include <volk/volk.h>

#include <tuple>
#include <limits>
#include <vector>
#include <stdexcept>

#include <cstdio>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstring>

#include <stb_image_write.h>

#include "../labutils/error.hpp"
#include "../labutils/vkutil.hpp"
#include "../labutils/vkobject.hpp"
#include "../labutils/to_string.hpp"
#include "../labutils/vulkan_context.hpp"
namespace lut = labutils;

#include "image.hpp"
#include "buffer.hpp"

namespace
{
	namespace cfg
	{
		// Image format:
		// Vulkan implementation do not have to support all image formats
		// listed in the VkFormat enum. Rather, some formats may only be used
		// with some operations. We are rendering (without blending!), so our
		// use case is VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT (with blending it
		// would be VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BLEND_BIT). We need a
		// format that supports VK_FORMAT_FEATURE_COLOR_ATTACHMENT_BIT (with
		// optimal tiling). Vulkan requires some formats to be supported - see
		// https://www.khronos.org/registry/vulkan/specs/1.3/html/chap34.html#features-required-format-support
		//
		// It turns out that support there are no mandatory formats listed when
		// rendering with blending disabled. However, it seems that commonly
		// allow R8G8B8A8_SRGB for this use case anyway:
		// http://vulkan.gpuinfo.org/listoptimaltilingformats.php
		constexpr VkFormat kImageFormat = VK_FORMAT_R8G8B8A8_SRGB;

		// Image size and related parameters
		constexpr std::uint32_t kImageWidth = 1280;
		constexpr std::uint32_t kImageHeight = 720;
		constexpr std::uint32_t kImageSize = kImageWidth * kImageHeight * 4; // RGBA

		// Output image path
		constexpr char const* kImageOutput = "D:/sc18jb/hpg/exercise12/output.png";

		// Compiled shader code for the graphics pipeline
		// See sources in exercise2/shaders/*. 
//#		define SHADERDIR_ "assets/exercise2/shaders/"
#		define SHADERDIR_ "D:/sc18jb/hpg/exercise12/exercise2/shaders/"
		constexpr char const* kVertShaderPath = SHADERDIR_ "triangle.vert.spv";
		constexpr char const* kFragShaderPath = SHADERDIR_ "triangle.frag.spv";
#		undef SHADERDIR_
	}

	// Helpers:
	lut::RenderPass create_render_pass( lut::VulkanContext const& );

	lut::PipelineLayout create_triangle_pipeline_layout( lut::VulkanContext const& );
	lut::Pipeline create_triangle_pipeline( lut::VulkanContext const&, VkRenderPass, VkPipelineLayout );


	std::tuple<Image,lut::ImageView> create_framebuffer_image( lut::VulkanContext const& );
	lut::Framebuffer create_framebuffer( lut::VulkanContext const&, VkRenderPass, VkImageView );

	Buffer create_download_buffer( lut::VulkanContext const& );

	void record_commands( 
		VkCommandBuffer,
		VkRenderPass,
		VkFramebuffer,
		VkPipeline,
		VkImage,
		VkBuffer
	);
	void submit_commands(
		lut::VulkanContext const&,
		VkCommandBuffer,
		VkFence
	);

	std::uint32_t find_memory_type( lut::VulkanContext const&, std::uint32_t aMemoryTypeBits, VkMemoryPropertyFlags );
}

int main() try
{
	// Create the Vulkan instance, set up the validation, select a physical
	// device and instantiate a logical device from the selected device.
	// Request a single GRAPHICS queue for now, and fetch this from the created
	// logical device.
	//
	// See Exercise 1.1 for a detailed walkthrough of this process.
	lut::VulkanContext context = lut::make_vulkan_context();

	// To render an image, we need a number of Vulkan resources. The following
	// creates these:
	lut::RenderPass renderPass = create_render_pass( context );

	lut::PipelineLayout pipeLayout = create_triangle_pipeline_layout( context );
	lut::Pipeline pipe = create_triangle_pipeline( context, renderPass.handle, pipeLayout.handle );

	auto [fbImage, fbImageView] = create_framebuffer_image( context );
	lut::Framebuffer framebuffer = create_framebuffer( context, renderPass.handle, fbImageView.handle );

	Buffer dlBuffer = create_download_buffer( context );

	lut::CommandPool cpool = lut::create_command_pool( context );
	VkCommandBuffer cbuffer = lut::alloc_command_buffer( context, cpool.handle );

	lut::Fence fence = lut::create_fence( context );

	// Now that we have set up the necessary resources, we can use our Vulkan
	// device to render the image. This happens in two steps:
	//  
	// 1. Record rendering commands in to the command buffer that we've 
	//    created for this purpose.
	// 2. Submit the command buffer to the Vulkan device / GPU for processing.

	record_commands(
		cbuffer,
		renderPass.handle,
		framebuffer.handle,
		pipe.handle,
		fbImage.image,
		dlBuffer.buffer
	);

	submit_commands(
		context,
		cbuffer,
		fence.handle
	);

	// Commands are executed asynchronously. Before we can access the result,
	// we need to wait for the processing to finish. The fence that we passed
	// to VkQueueSubmit() will become signalled when the command buffer has
	// finished processing -- we will wait for that to occur with
	// vkWaitForFences().

	// Wait for commands to finish executing
	constexpr std::uint64_t kMaxWait = std::numeric_limits<std::uint64_t>::max();
	if (auto const res = vkWaitForFences(context.device, 1, &fence.handle, VK_TRUE, kMaxWait); VK_SUCCESS != res)
	{
		throw lut::Error("Waiting for fence\n"
			"vkWaitForFences() returned % s", lut::to_string(res).c_str());
	}

	// Access image and write it to disk.
	// We copied the image to the buﬀer dlBuﬀer - we allocated the memory
	// backing this buﬀer, dlMemory, from a memory type with the property
	// VK MEMORY PROPERTY HOST VISIBLE BIT. This means that we can ”map” the
	// contents of this buﬀer such that it becomes accessible through a normal
	// C/C++ pointer.
	void* dataPtr = nullptr;
	if (auto const res = vkMapMemory(context.device, dlBuffer.memory, 0, cfg::
		kImageSize, 0, &dataPtr); VK_SUCCESS != res)
	{
			throw lut::Error("Mapping memory\n"
				"vkMapMemory() returned % s", lut::to_string(res).c_str());
	}


	assert(dataPtr);

	std::vector<std::byte> buffer(cfg::kImageSize);
	std::memcpy(buffer.data(), dataPtr, cfg::kImageSize);

	// Why the extra copy? dataPtr points into a special memory region. This
	// memory region may be created uncached (e.g., reads bypass CPU caches).
	// Streaming out of such memory is OK - so memcpy will touch each byte
	// exactly once. Reading multiple times from the memory, which the
	// compression method likely does, is signiﬁcantly more expensive.
	//
	// In my case: passing dataPtr directly to stbi write png() takes about
	// 4.5s, whereas using the extra buﬀer reduces this time to 0.5s.
	//
	// Instead of the extra copy, we could request memory that also has the
	// VK MEMORY PROPERTY HOST CACHED property. However, not all devices
	// support this, and it may have other overheads (i.e., device/driver
	// likely needs to snoop on the CPU caches, similar to HOST COHERENT).

	//vkUnmapMemory(context.device, dlBuffer.memory);

	//TODO: write image data to disk with stbi_write_png()
	
	if (!stbi_write_png(cfg::kImageOutput, cfg::kImageWidth, cfg::kImageHeight, 4, buffer.data(), cfg::kImageWidth * 4))
	{
		throw lut::Error("Unable to write image: stbi_write_png() returned error");
	}

	vkUnmapMemory(context.device, dlBuffer.memory);

	// Cleanup
	// None required :-) The C++ wrappers take care of destroying the various
	// objects as the wrappers go out of scpe at the end of main()!

	return 0;
}
catch( std::exception const& eErr )
{
	std::fprintf( stderr, "Exception: %s\n", eErr.what() );
	return 1;
}


namespace
{
	lut::RenderPass create_render_pass( lut::VulkanContext const& aContext )
	{
		// Note: the stencilLoadOp & stencilStoreOp members are left initialized
		// to 0 (=DONT CARE). The image format (R8G8B8A8 SRGB) of the color
		// attachment does not have a stencil component, so these are ignored
		// either way.
		VkAttachmentDescription attachments[1]{};
		attachments[0].format = cfg::kImageFormat; // VK FORMAT R8G8B8A8 SRGB
		attachments[0].samples = VK_SAMPLE_COUNT_1_BIT; // no multisampling
		attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
		attachments[0].finalLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
		
		VkAttachmentReference subpassAttachments[1]{};
		subpassAttachments[0].attachment = 0; // the zero refers to attachments[0] declared earlier.
		subpassAttachments[0].layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		VkSubpassDescription subpasses[1]{};
		subpasses[0].pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
		subpasses[0].colorAttachmentCount = 1;
		subpasses[0].pColorAttachments = subpassAttachments;
		// This subpass only uses a single color attachment, and does not use any
		// other attachmen types. We can therefore leave many of the members at
		// zero/nullptr. If this subpass used a depth attachment (=depth buﬀer),
		// we would specify this	ia the pDepthStencilAttachment member.
		//
		// See the documentation for VkSubpassDescription for other attachment
		// types and the use/meaning of those.

		VkSubpassDependency deps[1]{};
		deps[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;
		deps[0].srcSubpass = 0; // == subpasses[0] declared above
		deps[0].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		deps[0].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		deps[0].dstSubpass = VK_SUBPASS_EXTERNAL;
		deps[0].dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
		deps[0].dstStageMask = VK_PIPELINE_STAGE_TRANSFER_BIT;

		VkRenderPassCreateInfo passInfo{};
		passInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
		passInfo.attachmentCount = 1;
		passInfo.pAttachments = attachments;
		passInfo.subpassCount = 1;
		passInfo.pSubpasses = subpasses;
		passInfo.dependencyCount = 1;
		passInfo.pDependencies = deps;

		VkRenderPass rpass = VK_NULL_HANDLE;
		if (auto const res = vkCreateRenderPass(aContext.device, &passInfo, nullptr, &rpass); 
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create render pass\n"
				"vkCreateRenderPass() returned % s", lut::to_string(res).c_str()
			);
		}

		return lut::RenderPass(aContext.device, rpass);
		
	}


	lut::PipelineLayout create_triangle_pipeline_layout( lut::VulkanContext const& aContext )
	{
		VkPipelineLayoutCreateInfo layoutInfo{};
		layoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
		layoutInfo.setLayoutCount = 0;
		layoutInfo.pSetLayouts = nullptr;
		layoutInfo.pushConstantRangeCount = 0;
		layoutInfo.pPushConstantRanges = nullptr;
		VkPipelineLayout layout = VK_NULL_HANDLE;
		if (auto const res = vkCreatePipelineLayout(aContext.device, &layoutInfo, nullptr, &layout); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create pipeline layout\n"
				"vkCreatePipelineLayout() returned % s", lut::to_string(res).c_str()
			);
		}
		return lut::PipelineLayout(aContext.device, layout);

	}


	lut::Pipeline create_triangle_pipeline( lut::VulkanContext const& aContext, VkRenderPass aRenderPass, VkPipelineLayout aPipelineLayout )
	{
		// Load shader modules
		// For this example, we only use the	ertex and fragment shaders. Other
		// shader stages (geometry, tessellation) aren’t used here, and as such we
		// omit them.
		lut::ShaderModule vert = lut::load_shader_module(aContext, cfg::kVertShaderPath);
		lut::ShaderModule frag = lut::load_shader_module(aContext, cfg::kFragShaderPath);
		
		// Deﬁne shader stages in the pipeline
		VkPipelineShaderStageCreateInfo stages[2]{};
		stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
		stages[0].module = vert.handle;
		stages[0].pName = "main";

		stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
		stages[1].module = frag.handle;
		stages[1].pName = "main";

		// For now, we don’t have any vertex input attributes - the geometry is
		// generated/deﬁned in the	vertex shader.
		VkPipelineVertexInputStateCreateInfo inputInfo{};
		inputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

		// Deﬁne which primitive (point, line, triangle, ...) the input is
		// assembled into for rasterization.
		VkPipelineInputAssemblyStateCreateInfo assemblyInfo{};
		assemblyInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
		assemblyInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
		assemblyInfo.primitiveRestartEnable = VK_FALSE;

		// Deﬁne viewport and scissor regions
		VkViewport viewport{};
		viewport.x = 0.f;
		viewport.y = 0.f;
		viewport.width = float(cfg::kImageWidth);
		viewport.height = float(cfg::kImageHeight);
		viewport.minDepth = 0.f;
		viewport.maxDepth = 1.f;
		VkRect2D scissor{};
		scissor.offset = VkOffset2D{ 0, 0 };
		scissor.extent = VkExtent2D{ cfg::kImageWidth, cfg::kImageHeight };
		VkPipelineViewportStateCreateInfo	viewportInfo{};
		viewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
		viewportInfo.viewportCount = 1;
		viewportInfo.pViewports	= &viewport;
		viewportInfo.scissorCount = 1;
		viewportInfo.pScissors = &scissor;

		// Deﬁne rasterization options
		VkPipelineRasterizationStateCreateInfo rasterInfo{};
		rasterInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
		rasterInfo.depthClampEnable = VK_FALSE;
		rasterInfo.rasterizerDiscardEnable = VK_FALSE;
		rasterInfo.polygonMode = VK_POLYGON_MODE_FILL;
		rasterInfo.cullMode = VK_CULL_MODE_BACK_BIT;
		rasterInfo.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
		rasterInfo.depthBiasEnable = VK_FALSE;
		rasterInfo.lineWidth = 1.f; // required.

		// Deﬁne multisampling state
		VkPipelineMultisampleStateCreateInfo samplingInfo{};
		samplingInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
		samplingInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

		// Deﬁne blend state
		// We deﬁne one blend state per color attachment - this example uses a
		// single color attachment, so we only need one. Right now, we don’t do any
		// blending, so we can ignore most of the members.
		VkPipelineColorBlendAttachmentState blendStates[1]{};
		blendStates[0].blendEnable = VK_FALSE;
		blendStates[0].colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT
			| VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
		VkPipelineColorBlendStateCreateInfo blendInfo{};
		blendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
		blendInfo.logicOpEnable = VK_FALSE;
		blendInfo.attachmentCount = 1;
		blendInfo.pAttachments = blendStates;

		// Create pipeline
		VkGraphicsPipelineCreateInfo pipeInfo{};
		pipeInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
		pipeInfo.stageCount = 2;	// vertex + fragment stages
		pipeInfo.pStages = stages;

		pipeInfo.pVertexInputState = &inputInfo;
		pipeInfo.pInputAssemblyState = &assemblyInfo;
		pipeInfo.pTessellationState = nullptr; // no tessellation
		pipeInfo.pViewportState	= &viewportInfo;
		pipeInfo.pRasterizationState = &rasterInfo;
		pipeInfo.pMultisampleState = &samplingInfo;
		pipeInfo.pDepthStencilState = nullptr; // no depth or stencil buﬀers
		pipeInfo.pColorBlendState = &blendInfo;
		pipeInfo.pDynamicState = nullptr; // no dynamic states
		pipeInfo.layout = aPipelineLayout;
		pipeInfo.renderPass = aRenderPass;
		pipeInfo.subpass = 0; // ﬁrst subpass of aRenderPass


		VkPipeline pipe = VK_NULL_HANDLE;
		if (auto const res = vkCreateGraphicsPipelines(aContext.device, VK_NULL_HANDLE, 1,
			&pipeInfo, nullptr, &pipe); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create graphics pipeline\n"
				"vkCreateGraphicsPipelines() returned % s", lut::to_string(res).c_str()
			);
		}
		return lut::Pipeline(aContext.device, pipe);
	}


	std::tuple<Image, lut::ImageView> create_framebuffer_image( lut::VulkanContext const& aContext )
	{
		Image image( aContext.device );


		VkImageCreateInfo imageInfo{};
		imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
		imageInfo.imageType = VK_IMAGE_TYPE_2D;
		imageInfo.format = cfg::kImageFormat;
		imageInfo.extent = VkExtent3D{ cfg::kImageWidth, cfg::kImageHeight, 1 };
		imageInfo.mipLevels = 1;
		imageInfo.arrayLayers = 1;
		imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
		imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
		imageInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT |
			VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
		imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
		imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

		if (auto const res = vkCreateImage(aContext.device, &imageInfo, nullptr, &image.image); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create image\n"
				"vkCreateImage() returned % s", lut::to_string(res).c_str()
			);
		}

		VkMemoryRequirements memoryRequirements{};
		vkGetImageMemoryRequirements(aContext.device, image.image, &memoryRequirements);

		// after setting up memory // page 14-15
		VkMemoryAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memoryRequirements.size;
		allocInfo.memoryTypeIndex = find_memory_type(aContext, memoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
		if (auto const res = vkAllocateMemory(aContext.device, &allocInfo, nullptr, &image.memory); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to allocate memory for image\n"
				"vkAllocateMemory() returned % s", lut::to_string(res).c_str());
		}

		vkBindImageMemory(aContext.device, image.image, image.memory, 0);

		VkComponentMapping mapping;
		mapping.r = VK_COMPONENT_SWIZZLE_IDENTITY; // VK COMPONENT SWIZZLE R
		mapping.g = VK_COMPONENT_SWIZZLE_IDENTITY; // VK COMPONENT SWIZZLE G
		mapping.b = VK_COMPONENT_SWIZZLE_IDENTITY; // VK COMPONENT SWIZZLE B
		mapping.a = VK_COMPONENT_SWIZZLE_IDENTITY; // VK COMPONENT SWIZZLE A

		VkImageSubresourceRange range;
		range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		range.baseMipLevel = 0;
		range.levelCount = 1;
		range.baseArrayLayer = 0;
		range.layerCount = 1;

		VkImageViewCreateInfo viewInfo{};
		viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		viewInfo.image = image.image;
		viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		viewInfo.format = cfg::kImageFormat;
		viewInfo.components = mapping;
		viewInfo.subresourceRange = range;

		VkImageView	view = VK_NULL_HANDLE;

		if (auto const res = vkCreateImageView(aContext.device,	&viewInfo, nullptr,	&view); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create image view\n"
				"vkCreateImageView() returned % s", lut::to_string(res).c_str());
		}

		return { std::move(image), lut::ImageView(aContext.device, view) };

	}


	lut::Framebuffer create_framebuffer( lut::VulkanContext const& aContext, VkRenderPass aRenderPass, VkImageView aTargetImageView )
	{
		VkImageView attachments[1] = {
			aTargetImageView
		};

		VkFramebufferCreateInfo fbInfo{};
		fbInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		fbInfo.flags = 0; // normal framebuﬀer
		fbInfo.renderPass = aRenderPass;
		fbInfo.attachmentCount = 1;
		fbInfo.pAttachments = attachments;
		fbInfo.width = cfg::kImageWidth;
		fbInfo.height = cfg::kImageHeight;
		fbInfo.layers = 1;

		VkFramebuffer fb = VK_NULL_HANDLE;
		if (auto const res = vkCreateFramebuffer(aContext.device, &fbInfo, nullptr, &fb);
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create framebuffer\n"
				"vkCreateFramebuffer() returned % s", lut::to_string(res).c_str());
		}

		return lut::Framebuffer(aContext.device, fb);
	}

	Buffer create_download_buffer( lut::VulkanContext const& aContext )
	{
		Buffer buffer(aContext.device);
		VkBufferCreateInfo bufferInfo{};
		bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
		bufferInfo.size = cfg::kImageSize;
		bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT;
		bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

		if (auto const res = vkCreateBuffer(aContext.device, &bufferInfo, nullptr, &buffer.buffer); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to create buffer\n"
				"vkCreateBuffer() returned % s", lut::to_string(res).c_str());
		}
		
		// memory requirements
		VkMemoryRequirements memoryRequirements{};
		vkGetBufferMemoryRequirements(aContext.device, buffer.buffer, &memoryRequirements);

		VkMemoryAllocateInfo allocInfo{};
		allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
		allocInfo.allocationSize = memoryRequirements.size;
		allocInfo.memoryTypeIndex = find_memory_type(aContext, memoryRequirements.memoryTypeBits,
			VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT | VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
		if (auto const res = vkAllocateMemory(aContext.device, &allocInfo, nullptr, &buffer.memory); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to allocate memory for buffer\n"
				"vkAllocateMemory() returned % s", lut::to_string(res).c_str());
		}

		vkBindBufferMemory(aContext.device, buffer.buffer, buffer.memory, 0);

		return buffer;

	}

	void record_commands( VkCommandBuffer aCmdBuff, VkRenderPass aRenderPass, VkFramebuffer aFramebuffer, VkPipeline aGraphicsPipe, VkImage aFbImage, VkBuffer aDownloadBuffer )
	{
		// Begin recording commands
		VkCommandBufferBeginInfo begInfo{};
		begInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
		begInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

		begInfo.pInheritanceInfo = nullptr;
		if (auto const res = vkBeginCommandBuffer(aCmdBuff, &begInfo); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to begin recording command buffer\n"
				"vkBeginCommandBuffer() returned % s", lut::to_string(res).c_str());
		}


		// Begin render pass
		VkClearValue clearValues[1]{};
		clearValues[0].color.float32[0] = 0.1f; // Clear to a dark gray background.
		clearValues[0].color.float32[1] = 0.1f; // If we were debugging, this would potentially
		clearValues[0].color.float32[2] = 0.1f; // help us see whether the render pass took
		clearValues[0].color.float32[3] = 1.f; // place, even if nothing else was drawn.

		VkRenderPassBeginInfo passInfo{};
		passInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		passInfo.renderPass = aRenderPass;
		passInfo.framebuffer = aFramebuffer;
		passInfo.renderArea.offset = VkOffset2D{ 0, 0 };
		passInfo.renderArea.extent = VkExtent2D{ cfg::kImageWidth, cfg::kImageHeight };
		passInfo.clearValueCount = 1;
		passInfo.pClearValues = clearValues;

		vkCmdBeginRenderPass(aCmdBuff, &passInfo, VK_SUBPASS_CONTENTS_INLINE);

		// Begin drawing with our graphics pipeline
		vkCmdBindPipeline(aCmdBuff, VK_PIPELINE_BIND_POINT_GRAPHICS, aGraphicsPipe);

		// Draw a triangle = three vertices
		vkCmdDraw(aCmdBuff, 3, 1, 0, 0);

		// End the render pass
		vkCmdEndRenderPass(aCmdBuff);

		// Copy image to our download buﬀer
		VkImageSubresourceLayers layers{};
		layers.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		layers.mipLevel = 0;
		layers.baseArrayLayer = 0;
		layers.layerCount = 1;

		VkBufferImageCopy copy{};
		copy.bufferOffset = 0; // Note: these are initialized to zero already;
		copy.bufferRowLength = 0; // they’re listed here explicitly for purposes
		copy.bufferImageHeight = 0; // of showing them.
		copy.imageSubresource = layers;
		copy.imageOffset = VkOffset3D{ 0, 0, 0 }; // See comment above.
		copy.imageExtent = VkExtent3D{ cfg::kImageWidth, cfg::kImageHeight, 1 };

		vkCmdCopyImageToBuffer(aCmdBuff, aFbImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, aDownloadBuffer, 1, &copy);

		// End command recording
		if (auto const res = vkEndCommandBuffer(aCmdBuff); VK_SUCCESS != res)
		{
			throw lut::Error("Unable to end recording command buffer\n"
				"vkEndCommandBuffer() returned % s", lut::to_string(res).c_str());
		}
	}

	void submit_commands( lut::VulkanContext const& aContext, VkCommandBuffer aCmdBuff, VkFence aFence )
	{
		VkSubmitInfo subInfo{};
		subInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		subInfo.commandBufferCount = 1;
		subInfo.pCommandBuffers = &aCmdBuff;


		if (auto const res = vkQueueSubmit(aContext.graphicsQueue, 1, &subInfo, aFence);
			VK_SUCCESS != res)
		{
			throw lut::Error("Unable to submit command buffer to queue\n"
				"vkQueueSubmit() returned % s", lut::to_string(res).c_str());
		}
	}

}

namespace
{
	std::uint32_t find_memory_type( lut::VulkanContext const& aContext, std::uint32_t aMemoryTypeBits, VkMemoryPropertyFlags aProps )
	{
		// Based on ﬁndMemoryType() from
		// https://vulkan-tutorial.com/Vertex_buffers/Vertex_buffer_creation
		VkPhysicalDeviceMemoryProperties props;
		vkGetPhysicalDeviceMemoryProperties(aContext.physicalDevice, &props);
		for (std::uint32_t i = 0; i < props.memoryTypeCount; ++i)
		{
			auto const& type = props.memoryTypes[i];
			if (aProps == (aProps & type.propertyFlags) && (aMemoryTypeBits & (1u << i)))
			{
				return i;
			}
		}
		throw lut::Error("Unable to find suitable memory type (allowed memory types = 0x%x, required properties = % s)",
			aMemoryTypeBits, lut::memory_property_flags(aProps).c_str() );
	}
}

//EOF vim:syntax=cpp:foldmethod=marker:ts=4:noexpandtab: 
