#include <volk/volk.h>

#include <string>
#include <vector>
#include <optional>
#include <unordered_set>

#include <cstdio>
#include <cassert>
#include <cstdint>
#include <cstring>

#include "../labutils/to_string.hpp"
namespace lut = labutils;

namespace
{

	VkInstance create_instance();

	void enumerate_devices( VkInstance );

	float score_device( VkPhysicalDevice );
	VkPhysicalDevice select_device( VkInstance );

}

int main()
{
	// Use Volk to load the initial parts of the Vulkan API that are required
	// to create a Vulkan instance. This includes very few functions:
	// - vkGetInstanceProcAddr()
	// - vkCreateInstance()
	// - vkEnumerateInstanceExtensionProperties()
	// - vkEnumerateInstanceLayerProperties()
	// - vkEnumerateInstanceVersion() (added in Vulkan 1.1)
	if( auto const res = volkInitialize(); VK_SUCCESS != res )
	{
		std::fprintf( stderr, "Error: unable to load Vulkan API\n" );
		std::fprintf( stderr, "Volk returned error %s\n", lut::to_string(res).c_str() );
		return 1;
	}

	// We can use vkEnumerateInstanceVersion() to tell us the version of the
	// Vulkan loader. vkEnumerateInstanceVersion() was added in Vulkan 1.1, so
	// it might not be available on systems with particularly old Vulkan
	// loaders. In that case, assume the version is 1.0.x and output this.
	std::uint32_t loaderVersion = VK_MAKE_API_VERSION( 0, 1, 0, 0 );
	if( vkEnumerateInstanceVersion )
	{
		if( auto const res = vkEnumerateInstanceVersion( &loaderVersion ); VK_SUCCESS != res )
		{
			std::fprintf( stderr, "Warning: vkEnumerateInstanceVersion() returned error %s\n", lut::to_string(res).c_str() );
		}
	}

	std::printf( "Vulkan loader version: %d.%d.%d (variant %d)\n", VK_API_VERSION_MAJOR(loaderVersion), VK_API_VERSION_MINOR(loaderVersion), VK_API_VERSION_PATCH(loaderVersion), VK_API_VERSION_VARIANT(loaderVersion) );


	// Create Vulkan instance
	VkInstance instance = create_instance();

	if( VK_NULL_HANDLE == instance )
		return 1;

	// Instruct Volk to load the remainder of the Vulkan API.
	volkLoadInstance( instance );


	// Print Vulkan devices
	enumerate_devices( instance );

	// Select appropriate Vulkan device
	VkPhysicalDevice physicalDevice = select_device( instance );
	if( VK_NULL_HANDLE == physicalDevice )
	{
		vkDestroyInstance( instance, nullptr );

		std::fprintf( stderr, "Error: no suitable physical device found!\n" );
		return 1;
	}

	{
		VkPhysicalDeviceProperties props;
		vkGetPhysicalDeviceProperties( physicalDevice, &props );
		std::printf( "Selected device: %s\n", props.deviceName );
	}


	// Cleanup

	vkDestroyInstance( instance, nullptr );
	
	return 0;
}

namespace
{
	VkInstance create_instance()
	{
		return VK_NULL_HANDLE;
	}

}



namespace
{
	float score_device( VkPhysicalDevice aPhysicalDev )
	{
		return -1.f;
	}
	
	VkPhysicalDevice select_device( VkInstance aInstance )
	{
		assert( VK_NULL_HANDLE != aInstance );

		std::vector<VkPhysicalDevice> devices; // TODO!

		float bestScore = -1.f;
		VkPhysicalDevice bestDevice = VK_NULL_HANDLE;

		for( auto const device : devices )
		{
			auto const score = score_device( device );
			if( score > bestScore )
			{
				bestScore = score;
				bestDevice = device;
			}
		}

		return bestDevice;
	}

	void enumerate_devices( VkInstance aInstance )
	{
		assert( VK_NULL_HANDLE != aInstance );

	}
}

//EOF vim:syntax=cpp:foldmethod=marker:ts=4:noexpandtab: 
