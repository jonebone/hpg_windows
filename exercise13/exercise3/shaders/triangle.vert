#version 450


const vec2 kVertexPositions[3] = vec2[3] (
	vec2(0., .8),
	vec2(-0.7, .8),
	vec2(.7, .8)
);

const vec2 jVertexPositions[3] = vec2[3] (
	vec2(0., 0.0),
	vec2(0., 1.),
	vec2(1., 0.)
);

void main()
{
	const vec2 xy = jVertexPositions[gl_VertexIndex];
	gl_Position = vec4(xy, 0.5f, 1.f);
}
