#version 450

layout (location = 0) in vec3 iPosition;
layout (location = 1) in vec3 iColor;
layout (location = 2) in vec2 iTexCoord;
layout (location = 3) in int iTexID;
layout (location = 4) in vec3 iNormal;

layout (set = 0, binding = 0) uniform UScene {
	mat4 camera;
	mat4 projection;
	mat4 projCam;
	mat4 lightAnim;
	vec4 cameraPos;
	vec4 ambientColor;
} uScene;

layout (location = 0) out vec3 v2fPosition;
layout (location = 1) out vec3 v2fColor;
layout (location = 2) out vec2 v2fTexCoord;
layout (location = 3) out int v2fTexID;
layout (location = 4) out vec3 v2fNormal;

void main()
{
	mat4 translationMat = mat4(1.); // identity matrix
	// set last col to be x, y, z translation
	//translationMat[3] = vec4(10., 0., 10., 1.);
	mat4 modelMat = mat4(1.) * translationMat;

	gl_Position = uScene.projCam * modelMat * vec4(iPosition, 1.);

	

	v2fColor = iColor;
	v2fTexCoord = iTexCoord;
	v2fTexID = iTexID;
	v2fPosition = iPosition;
	v2fNormal = iNormal;
	//v2fPosition = (modelMat * vec4(iPosition, 1.)).xyz;
	//v2fNormal = (modelMat * vec4(iNormal, 0.)).xyz;
}