#version 450
#extension GL_EXT_nonuniform_qualifier : enable

#define M_PI 3.14159265358979323


layout (location = 0) in vec3 v2fPosition;
layout (location = 1) in vec3 v2fColor;
layout (location = 2) in vec2 v2fTexCoord;
layout (location = 3) flat in int v2fTexID;
layout (location = 4) in vec3 v2fNormal;

// scene info:
layout (set = 0, binding = 0) uniform UScene {
	mat4 camera;
	mat4 projection;
	mat4 projCam;
	vec4 cameraPos; // hid the number of lights as the 4th term
	vec4 ambientLightColor;
} uScene;


// mixed material:
layout( set = 1, binding = 0 ) uniform UMaterial
{
	vec4 emissive;
	vec4 albedo;
	float shininess;
	float metalness;
} uMaterial[]; // array of materials since I can do dynamic indexing

// need a second set for lights since you can't have dynamic arrays in the same set
// lights :
layout( set = 2, binding = 0 ) uniform ULight
{
	vec4 position;
	vec4 color;
} uLight[]; // dynamic array of lights

layout (location = 0) out vec4 fragColor;

vec3 specular_base(vec4 col, float M) {
	return (1. - M) * vec3(0.04) + M * col.rgb;
}

vec3 Fresnel_term(vec3 sb, vec3 h, vec3 v) {
	return sb + (vec3(1.) - sb) * pow((1. - dot(h, v)), 5.);
}

vec3 Lambertian_diffuse(vec3 F, float M, vec4 col) {
	return (col.rgb / M_PI) * (1. - F) * (1. - M);
}

float BP_distro(float shine, float NdotH) {
	return ((shine + 2.) / (2. * M_PI)) * max(0., pow(NdotH, shine));
}

float CT_mask_unclamped(vec3 l, vec3 v, vec3 h, vec3 n) {
	float mult = 2. * dot(n, h) / dot(v, h);
	return min(1., min(mult * dot(n, v), mult * dot(n, l) ) );
}

float CT_mask(vec3 l, vec3 v, vec3 h, vec3 n) {
	float NdotV = clamp(dot(n, v), 0., 1.);
	float NdotL = clamp(dot(n, l), 0., 1.);
	float NdotH = clamp(dot(n, h), 0., 1.);
	float VdotH = clamp(dot(v, h), 0., 1.);
	float mult = 2. * dot(n, h) / dot(v, h);
	mult = 2. * NdotH / dot(v, h);
	return min(1., min(mult * NdotV, mult * NdotL ) );
	return min(1., min(mult * dot(n, v), mult * dot(n, l) ) );
}

vec4 PBR_light_contribution(int index, vec3 viewDir, vec3 nDir, vec3 sb, vec4 aCol, float NdotV, float metal, float shine) {
	vec3 lightDir = normalize(uLight[nonuniformEXT(index)].position.xyz - v2fPosition);

	vec4 lColor = uLight[nonuniformEXT(index)].color;
	
	vec3 hDir = normalize(lightDir + viewDir);
	float NdotH = clamp(dot(nDir, hDir), 0., 1.);
	float NdotL = clamp(dot(nDir, lightDir), 0., 1.);

	vec3 fresnel = Fresnel_term(sb, hDir, viewDir);
	vec3 lambertian = Lambertian_diffuse(fresnel, metal, aCol);
	float bpDistro = BP_distro(shine, NdotH);
	//float ctMask = CT_mask_unclamped(lightDir, viewDir, hDir, nDir);
	//float unclamped_mask = CT_mask_unclamped(lightDir, viewDir, hDir, nDir);
	float ctMask = CT_mask(lightDir, viewDir, hDir, nDir);
	//ctMask = unclamped_mask;
	
	//return vec4(fresnel, 1.);
	//return vec4(bpDistro);
	//return vec4(unclamped_mask);
	//return vec4(ctMask);

	
	//return lColor * vec4((bpDistro * fresnel * ctMask)/(4. * dot(nDir, viewDir) * dot(nDir, lightDir)), 1.) * NdotL;
	//return lColor * vec4((bpDistro * fresnel * ctMask)/(4. * NdotV * NdotL), 1.);
	// don't clamp in division term so that we avoid div by zero
	return lColor * vec4(lambertian + (bpDistro * fresnel * ctMask)/(4. * dot(nDir, viewDir) * dot(nDir, lightDir) + 0.001), 1.) * NdotL;
}

vec4 bp_light_contribution(int index, vec3 viewDir, vec3 nDir, vec4 diffuse, vec4 specular, float shine) {
	vec4 lColor = uLight[nonuniformEXT(index)].color;
	vec3 lightDir = normalize(uLight[nonuniformEXT(index)].position.xyz - v2fPosition);
	vec3 hDir = normalize(lightDir + viewDir);
	float NdotL = clamp(dot(nDir, lightDir), 0., 1.);
	float shineTerm = (shine + 2.)/8.;
	float NdotH = clamp(dot(nDir, hDir), 0., 1.);

	return lColor * NdotL * ((diffuse / M_PI) + shineTerm * max(0., pow(NdotH, shine)) * specular);
}

void main()
{
	vec3 viewDir = normalize(uScene.cameraPos.xyz - v2fPosition);
	vec3 nDir = normalize(v2fNormal);
	vec4 albedo = uMaterial[nonuniformEXT(v2fTexID)].albedo.rgba;
	float metalness = uMaterial[nonuniformEXT(v2fTexID)].metalness;
	float shininess = uMaterial[nonuniformEXT(v2fTexID)].shininess;
	float NdotV = clamp(dot(nDir, viewDir), 0., 1.);

	vec3 sb = specular_base(albedo, metalness);

	fragColor = vec4(uMaterial[nonuniformEXT(v2fTexID)].emissive.rgb, 1.);
	fragColor += albedo * uScene.ambientLightColor;

	//fragColor = vec4(0.);

	// hid the number of lights as the 4th term of camera pos
	// use with max so that in 0 case we have 1 still
	int lightCount = max( 1, int(uScene.cameraPos.w)); 
	//lightCount = 3;
	

	for (int i = 0; i < lightCount; i++) {
		fragColor += PBR_light_contribution(i, viewDir, nDir, sb, albedo, NdotV, metalness, shininess);
	}
	// fragColor += everything else;


}

