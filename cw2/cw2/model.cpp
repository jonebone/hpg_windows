﻿#include "model.hpp"

#include <utility>

#include <cstdio>
#include <cassert>

#include <cstring> // for std::memcpy()

#include "../labutils/vkutil.hpp"
#include "../labutils/vkbuffer.hpp"
#include "../labutils/to_string.hpp"
#include "../labutils/error.hpp"
namespace lut = labutils;

// ModelData
ModelData::ModelData() noexcept = default;

ModelData::ModelData( ModelData&& aOther ) noexcept
	: modelName( std::exchange( aOther.modelName, {} ) )
	, modelSourcePath( std::exchange( aOther.modelSourcePath, {} ) )
	, materials( std::move( aOther.materials ) )
	, meshes( std::move( aOther.meshes ) )
	, vertexPositions( std::move( aOther.vertexPositions ) )
	, vertexNormals( std::move( aOther.vertexNormals ) )
	, vertexTextureCoords( std::move( aOther.vertexTextureCoords ) )
{}

ModelData& ModelData::operator=( ModelData&& aOther ) noexcept
{
	std::swap( modelName, aOther.modelName );
	std::swap( modelSourcePath, aOther.modelSourcePath );
	std::swap( materials, aOther.materials );
	std::swap( meshes, aOther.meshes );
	std::swap( vertexPositions, aOther.vertexPositions );
	std::swap( vertexNormals, aOther.vertexNormals );
	std::swap( vertexTextureCoords, aOther.vertexTextureCoords );
	return *this;
}


// load_obj_model()
ModelData load_obj_model( std::string_view const& aOBJPath )
{
	// "Decode" path
	std::string fileName, directory;

	if( auto const separator = aOBJPath.find_last_of( "/\\" ); std::string_view::npos != separator )
	{
		fileName = aOBJPath.substr( separator+1 );
		directory = aOBJPath.substr( 0, separator+1 );
	}
	else
	{
		fileName = aOBJPath;
		directory = "./";
	}

	std::string const normalizedPath = directory + fileName;

	// Load model
	std::printf( "Loading: '%s' ...", normalizedPath.c_str() );
	std::fflush( stdout );

	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string err;

	if( !tinyobj::LoadObj( &attrib, &shapes, &materials, &err, normalizedPath.c_str(), directory.c_str(), true ) )
	{
		throw lut::Error( "Unable to load OBJ '%s':\n%s", normalizedPath.c_str(), err.c_str() );
	}

	// Apparently this can include some warnings:
	if( !err.empty() )
		std::printf( "\n%s\n... OK", err.c_str() );
	else
		std::printf( " OK\n" );

	// Transfer into our ModelData structures
	ModelData model;
	model.modelName        = aOBJPath;
	model.modelSourcePath  = normalizedPath;

	// ... copy over material data ...
	for( auto const& m : materials )
	{
		MaterialInfo info{};
		info.materialName      = m.name;

		info.color  = glm::vec3( m.diffuse[0], m.diffuse[1], m.diffuse[2] );

		info.emissive  = glm::vec3( m.emission[0], m.emission[1], m.emission[2] );
		info.diffuse  = glm::vec3( m.diffuse[0], m.diffuse[1], m.diffuse[2] );
		info.specular  = glm::vec3( m.specular[0], m.specular[1], m.specular[2] );
		info.shininess  = m.roughness;

		info.albedo  = glm::vec3( m.diffuse[0], m.diffuse[1], m.diffuse[2] );
		info.metalness  = m.metallic;

		model.materials.emplace_back( info );
	}

	// ... copy over mesh data ...
	// Note: this converts the mesh into a triangle soup. OBJ meshes use
	// separate indices for vertex positions, texture coordinates and normals.
	// This is not compatible with the default draw modes of OpenGL or Vulkan,
	// where each vertex has a single index that refers to all attributes.
	//
	// tinyobjloader additionally complicates the situation by specifying a
	// per-face material indices, which is rather impractical.
	//
	// In short- The OBJ format isn't exactly a great format (in a modern
	// context), and tinyobjloader is not making the situation a lot better.
	std::size_t totalVertices = 0;
	for( auto const& s : shapes )
	{
		totalVertices += s.mesh.indices.size();
	}

	model.vertexPositions.reserve( totalVertices );
	model.vertexNormals.reserve( totalVertices );
	model.vertexTextureCoords.reserve( totalVertices );

	std::size_t currentIndex = 0;
	for( auto const& s : shapes )
	{
		auto const& objMesh = s.mesh;

		if( objMesh.indices.empty() )
			continue;

		assert( !objMesh.material_ids.empty() );

		// The OBJ format allows for general polygons and not just triangles.
		// However, this code only deals with triangles. Check that we only
		// have triangles (debug mode only).
		assert( objMesh.indices.size() % 3 == 0 );
#		ifndef NDEBUG
		for( auto faceVerts : objMesh.num_face_vertices )
			assert( 3 == faceVerts );
#		endif // ~ NDEBUG

		// Each of our rendered objMeshes can only have a single material. 
		// Split the OBJ objMesh into multiple objMeshes if there are multiple 
		// materials. 
		//
		// Note: if a single material is repeated multiple times, this will
		// generate a new objMesh for each time the material is encountered.
		int currentMaterial = -1; // start a new material!

		std::size_t indices = 0;

		std::size_t face = 0, vert = 0;
		for( auto const& objIdx : objMesh.indices )
		{
			// check if material changed
			auto const matId = objMesh.material_ids[face];
			if( matId != currentMaterial )
			{
				if( indices )
				{
					assert( currentMaterial >= 0 ); 

					MeshInfo mesh{};
					mesh.materialIndex     = currentMaterial;
					mesh.meshName          = s.name + "::" + model.materials[currentMaterial].materialName;
					mesh.vertexStartIndex  = currentIndex;
					mesh.numberOfVertices  = indices;

					model.meshes.emplace_back( mesh );
				}

				currentIndex += indices;
				currentMaterial = matId;
				indices = 0;
			}

			// copy over data
			model.vertexPositions.emplace_back( glm::vec3(
				attrib.vertices[ objIdx.vertex_index * 3 + 0 ],
				attrib.vertices[ objIdx.vertex_index * 3 + 1 ],
				attrib.vertices[ objIdx.vertex_index * 3 + 2 ]
			) );

			assert( objIdx.normal_index >= 0 ); // must have a normal!
			model.vertexNormals.emplace_back( glm::vec3(
				attrib.normals[ objIdx.normal_index * 3 + 0 ],
				attrib.normals[ objIdx.normal_index * 3 + 1 ],
				attrib.normals[ objIdx.normal_index * 3 + 2 ]
			) );

			if( objIdx.texcoord_index >= 0 )
			{
				model.vertexTextureCoords.emplace_back( glm::vec2(
					attrib.texcoords[ objIdx.texcoord_index * 2 + 0 ],
					attrib.texcoords[ objIdx.texcoord_index * 2 + 1 ]
				) );
			}
			else
			{
				model.vertexTextureCoords.emplace_back( glm::vec2( 0.f, 0.f ) );
			}

			++indices;

			// accounting: next vertex
			++vert;
			if( 3 == vert )
			{
				++face;
				vert = 0;
			}
		}

		if( indices )
		{
			assert( currentMaterial >= 0 ); 

			MeshInfo mesh{};
			mesh.materialIndex     = currentMaterial;
			mesh.meshName          = s.name + "::" + model.materials[currentMaterial].materialName;
			mesh.vertexStartIndex  = currentIndex;
			mesh.numberOfVertices  = indices;

			currentIndex += indices;

			model.meshes.emplace_back( mesh );
		}
	}

	assert( model.vertexPositions.size() == totalVertices );
	assert( model.vertexNormals.size() == totalVertices );
	assert( model.vertexTextureCoords.size() == totalVertices );
	
	return model;
}

BufferMesh create_buffer_mesh(ModelData& aModel, labutils::VulkanContext const& aContext, labutils::Allocator const& aAllocator) {
	/// Vertex data
	// vertexPositions is a vector of vec3's so 1:1 :: (vector size):(vertex count)
	uint32_t vertexCount = aModel.vertexPositions.size();

	// make float arrays from ModelData vectors of vec3's
	// glm vec3's and std::vectors are contiguously packed
	// so we can set array pointer to the pointer to the first element of the first vec3 in the corresponding ModelData vector
	float* positions = &aModel.vertexPositions[0][0];
	float* texcoord = &aModel.vertexTextureCoords[0][0];
	float* normals = &aModel.vertexNormals[0][0];

	// populate color and texid vectors to give per-vertex info
	std::vector<glm::vec3> colorVec = std::vector<glm::vec3>();
	std::vector<int32_t> texids = std::vector<int32_t>();

	int mc = 0;
	for (MeshInfo m : aModel.meshes) {
		// newSize can be same for both, since colors is vec of vec3, one vec3 per vertex, instead of 3 floats per vertex
		size_t newSize = colorVec.size() + m.numberOfVertices;
		uint32_t meshMaterialID = m.materialIndex;
		glm::vec3 meshColor = aModel.materials[meshMaterialID].color;
		// tex ID is -1 if no texture
		//int32_t meshTexID = aModel.materials[meshMaterialID].colorTexturePath.empty() ? -1 : meshMaterialID;

		// second arg of resize is fill value for positions past current size
		// and since new size is colors.size() + m.numberOfVertices,
		// there will be m.numberOfVertices new entries created at the end, 
		// each with the corresponding mesh color or tex ID
		colorVec.resize(newSize, meshColor);
		texids.resize(newSize, meshMaterialID);

		//std::string str = aModel.materials[meshMaterialID].colorTexturePath.empty() ? "empty" : aModel.materials[meshMaterialID].colorTexturePath;

		//std::printf("mesh %i is using material %i : color is %f %f %f\n", mc, meshMaterialID, meshColor.x, meshColor.y, meshColor.z);
		//std::printf("mesh %s is using material %s : color is %f %f %f\n", m.meshName.c_str() , aModel.materials[meshMaterialID].materialName.c_str(), meshColor.x, meshColor.y, meshColor.z);
		//std::printf("mesh %i is using material %i : texture is %s\n", mc, meshMaterialID, str.c_str());
		//std::printf("mesh %i is using material %i : texture index is %i\n", mc, meshMaterialID, meshTexID); //aModel.materials[meshMaterialID].materialName.c_str()
		mc++;
	}

	//for (int i : texids) {
	//	std::printf("%i ", i);
	//}
	//std::printf("\n");

	// color flat float array
	float* colors = &colorVec[0][0];
	// texID flat uint32_t array is just texids.data()


	// largely based on exercise material: 

	// Create ﬁnal GPU buﬀers
	lut::Buffer	vertexPosGPU = lut::create_buffer(
		aAllocator,
		sizeof(float) * vertexCount * 3,
		VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		VMA_MEMORY_USAGE_GPU_ONLY
	);

	lut::Buffer	vertexColGPU = lut::create_buffer(
		aAllocator,
		sizeof(float) * vertexCount * 3,
		VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		VMA_MEMORY_USAGE_GPU_ONLY
	);

	lut::Buffer	vertexTexGPU = lut::create_buffer(
		aAllocator,
		sizeof(float) * vertexCount * 2,
		VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		VMA_MEMORY_USAGE_GPU_ONLY
	);

	lut::Buffer	vertexTexIDsGPU = lut::create_buffer(
		aAllocator,
		sizeof(int) * vertexCount,
		VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		VMA_MEMORY_USAGE_GPU_ONLY
	);

	lut::Buffer	vertexNormGPU = lut::create_buffer(
		aAllocator,
		sizeof(float) * vertexCount * 3,
		VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
		VMA_MEMORY_USAGE_GPU_ONLY
	);

	// staging buffers (on CPU)
	lut::Buffer posStaging = lut::create_buffer(
		aAllocator,
		sizeof(float) * vertexCount * 3,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VMA_MEMORY_USAGE_CPU_TO_GPU
	);

	lut::Buffer colStaging = lut::create_buffer(
		aAllocator,
		sizeof(float) * vertexCount * 3,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VMA_MEMORY_USAGE_CPU_TO_GPU
	);

	lut::Buffer texStaging = lut::create_buffer(
		aAllocator,
		sizeof(float) * vertexCount * 2,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VMA_MEMORY_USAGE_CPU_TO_GPU
	);

	lut::Buffer texIDsStaging = lut::create_buffer(
		aAllocator,
		sizeof(int) * vertexCount,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VMA_MEMORY_USAGE_CPU_TO_GPU
	);

	lut::Buffer normStaging = lut::create_buffer(
		aAllocator,
		sizeof(float) * vertexCount * 3,
		VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
		VMA_MEMORY_USAGE_CPU_TO_GPU
	);

	// copy data into CPU buffers
	void* posPtr = nullptr;
	if (auto const res = vmaMapMemory(aAllocator.allocator, posStaging.allocation, &posPtr); VK_SUCCESS != res)
	{
		throw lut::Error("Mapping memory for writing\n"
			"vmaMapMemory() returned %s", lut::to_string(res).c_str());
	}
	std::memcpy(posPtr, positions, sizeof(float) * vertexCount * 3);
	vmaUnmapMemory(aAllocator.allocator, posStaging.allocation);

	void* colPtr = nullptr;
	if (auto const res = vmaMapMemory(aAllocator.allocator, colStaging.allocation, &colPtr); VK_SUCCESS != res)
	{
		throw lut::Error("Mapping memory for writing\n"
			"vmaMapMemory() returned %s", lut::to_string(res).c_str());
	}
	std::memcpy(colPtr, colors, sizeof(float) * vertexCount * 3);
	vmaUnmapMemory(aAllocator.allocator, colStaging.allocation);

	void* texPtr = nullptr;
	if (auto const res = vmaMapMemory(aAllocator.allocator, texStaging.allocation, &texPtr); VK_SUCCESS != res)
	{
		throw lut::Error("Mapping memory for writing\n"
			"vmaMapMemory() returned %s", lut::to_string(res).c_str());
	}
	std::memcpy(texPtr, texcoord, sizeof(float) * vertexCount * 2);
	vmaUnmapMemory(aAllocator.allocator, texStaging.allocation);

	void* texIDsPtr = nullptr;
	if (auto const res = vmaMapMemory(aAllocator.allocator, texIDsStaging.allocation, &texIDsPtr); VK_SUCCESS != res)
	{
		throw lut::Error("Mapping memory for writing\n"
			"vmaMapMemory() returned %s", lut::to_string(res).c_str());
	}
	std::memcpy(texIDsPtr, texids.data(), sizeof(int) * vertexCount);
	vmaUnmapMemory(aAllocator.allocator, texIDsStaging.allocation);

	void* normPtr = nullptr;
	if (auto const res = vmaMapMemory(aAllocator.allocator, normStaging.allocation, &normPtr); VK_SUCCESS != res)
	{
		throw lut::Error("Mapping memory for writing\n"
			"vmaMapMemory() returned %s", lut::to_string(res).c_str());
	}
	std::memcpy(normPtr, normals, sizeof(float) * vertexCount * 3);
	vmaUnmapMemory(aAllocator.allocator, normStaging.allocation);

	// We need to ensure that the Vulkan resources are alive until all the
	// transfers have completed. For simplicity, we will just wait for the
	// operations to complete with a fence. A more complex solution might want
	// to queue transfers, let these take place in the background while
	// performing other tasks.
	lut::Fence uploadComplete = create_fence(aContext);
	// Queue data uploads from staging buﬀers to the ﬁnal buﬀers
	// This uses a separate command pool for simplicity.
	lut::CommandPool uploadPool = create_command_pool(aContext);
	VkCommandBuffer uploadCmd = alloc_command_buffer(aContext, uploadPool.handle);

	// record copy commands into command buffer
	VkCommandBufferBeginInfo beginInfo{};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = 0;
	beginInfo.pInheritanceInfo = nullptr;
	if (auto const res = vkBeginCommandBuffer(uploadCmd, &beginInfo); VK_SUCCESS != res)
	{
		throw lut::Error("Beginning command buffer recording\n"
			"vkBeginCommandBuffer() returned %s", lut::to_string(res).c_str());
	}

	// copying CPU buffer data to GPU
	// pos copy
	VkBufferCopy pcopy{};
	pcopy.size = sizeof(float) * vertexCount * 3;

	vkCmdCopyBuffer(uploadCmd, posStaging.buffer, vertexPosGPU.buffer, 1, &pcopy);

	lut::buffer_barrier(uploadCmd,
		vertexPosGPU.buffer,
		VK_ACCESS_TRANSFER_WRITE_BIT,
		VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
		VK_PIPELINE_STAGE_TRANSFER_BIT,
		VK_PIPELINE_STAGE_VERTEX_INPUT_BIT
	);
	// color copy
	VkBufferCopy ccopy{};
	ccopy.size = sizeof(float) * vertexCount * 3;

	vkCmdCopyBuffer(uploadCmd, colStaging.buffer, vertexColGPU.buffer, 1, &ccopy);

	lut::buffer_barrier(uploadCmd,
		vertexColGPU.buffer,
		VK_ACCESS_TRANSFER_WRITE_BIT,
		VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
		VK_PIPELINE_STAGE_TRANSFER_BIT,
		VK_PIPELINE_STAGE_VERTEX_INPUT_BIT
	);
	// texture coord copy
	VkBufferCopy tcopy{};
	tcopy.size = sizeof(float) * vertexCount * 2;

	vkCmdCopyBuffer(uploadCmd, texStaging.buffer, vertexTexGPU.buffer, 1, &tcopy);

	lut::buffer_barrier(uploadCmd,
		vertexTexGPU.buffer,
		VK_ACCESS_TRANSFER_WRITE_BIT,
		VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
		VK_PIPELINE_STAGE_TRANSFER_BIT,
		VK_PIPELINE_STAGE_VERTEX_INPUT_BIT
	);
	// texture ID copy
	VkBufferCopy tidcopy{};
	tidcopy.size = sizeof(int) * vertexCount;

	vkCmdCopyBuffer(uploadCmd, texIDsStaging.buffer, vertexTexIDsGPU.buffer, 1, &tidcopy);

	lut::buffer_barrier(uploadCmd,
		vertexTexIDsGPU.buffer,
		VK_ACCESS_TRANSFER_WRITE_BIT,
		VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
		VK_PIPELINE_STAGE_TRANSFER_BIT,
		VK_PIPELINE_STAGE_VERTEX_INPUT_BIT
	);
	// normals copy
	VkBufferCopy ncopy{};
	ncopy.size = sizeof(float) * vertexCount * 3;

	vkCmdCopyBuffer(uploadCmd, normStaging.buffer, vertexNormGPU.buffer, 1, &ncopy);

	lut::buffer_barrier(uploadCmd,
		vertexNormGPU.buffer,
		VK_ACCESS_TRANSFER_WRITE_BIT,
		VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
		VK_PIPELINE_STAGE_TRANSFER_BIT,
		VK_PIPELINE_STAGE_VERTEX_INPUT_BIT
	);

	// end command buffer
	if (auto const res = vkEndCommandBuffer(uploadCmd); VK_SUCCESS != res)
	{
		throw lut::Error("Ending command buffer recording\n"
			"vkEndCommandBuffer() returned %s", lut::to_string(res).c_str()
		);
	}

	// Submit transfer commands
	VkSubmitInfo submitInfo{};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &uploadCmd;
	if (auto const res = vkQueueSubmit(aContext.graphicsQueue, 1, &submitInfo, uploadComplete.handle);
		VK_SUCCESS != res)
	{
		throw lut::Error("Submitting commands\n"
			"vkQueueSubmit() returned %s", lut::to_string(res).c_str());
	}

	// Wait for commands to ﬁnish before we destroy the temporary resources
	// required for the transfers (staging buﬀers, command pool, ...)
	//
	// The code doesn’t destory the resources implicitly – the resources are
	// destroyed by the destructors of the labutils wrappers for the	arious
	// objects once we leave the function’s scope.
	if (auto const res = vkWaitForFences(aContext.device, 1, &uploadComplete.handle, VK_TRUE, std::numeric_limits<std::uint64_t>::max());
		VK_SUCCESS != res)
	{
		throw lut::Error("Waiting for upload to complete\n"
			"vkWaitForFences() returned % s", lut::to_string(res).c_str());
	}


	return BufferMesh{
		std::move(vertexPosGPU),
		std::move(vertexColGPU),
		std::move(vertexTexGPU),
		std::move(vertexTexIDsGPU),
		std::move(vertexNormGPU),
		vertexCount
	};
}