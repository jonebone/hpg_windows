# CW2

## changes from cw1
I replaced the sampler descriptor set array with a material buffer descriptor set array, still using dynamic indexing. Lights were added as a second descriptor set array, using set 2 instead of another binding since I could only have one dynamic size array per set (if seeing 'set' so much gets confusing, then its true to the experiance). I also added camera position and ambient light level to the scene uniforms, and made the scene uniforms accessible in the frag shader.

## Descriptor layout
0. set 0 : scene uniforms, this set accessable to both vert and frag shaders
1. set 1 : dynamic array of materials, replacing the dynamic array of textures
2. set 2 : dynamic array of lights, this made more sense than passing one descriptor containing an array as it made the upper limit much higher
seperate sets are needed for each aspect, as dynamic arrays of descriptors like to each have their own sets

## shading space
all lighting calculations are done in world space, which is optimal for point lights and camera since it requires no transformations to be done to each thing

## animated light
I used glm to create a simple rotation matrix based on glfwGetTime as the angle, so all lights move in a circle around the Y axis

## multiple light sources
I made the UI support as many light sources as could be added by making the number keys turn on a percentage of the max number of lights. 1 turns on 1 light, and then 2-9 turn on 20%-90% of lights, accordingly, and then 0 turns on 100% of lights.

## custom PBR
I only modified the normal distribution, implementing GGX as well as the Henyey-Greenstein phase function. The Henyey-Greenstein function is made for working with small particles, but Burley 2012 observed that it produced similar results to GGX when the bottom term of GGX was raised to 1.5 instead of 2.

Equations and sources for the new models are in the comments of the PBR shader where they are used

