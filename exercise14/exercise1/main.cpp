﻿#include <volk/volk.h>

#include <string>
#include <vector>
#include <optional>
#include <unordered_set>

#include <cstdio>
#include <cassert>
#include <cstdint>
#include <cstring>

#include "../labutils/to_string.hpp"
namespace lut = labutils;


namespace
{
	std::unordered_set<std::string> get_instance_layers();
	std::unordered_set<std::string> get_instance_extensions();
	//VkInstance create_instance(); // old declaration
	VkInstance create_instance(
		std::vector<char const*> const& aEnabledLayers = {},
		std::vector<char const*> const& aEnabledInstanceExtensions = {},
		bool aEnableDebugUtils = false
	);

	void enumerate_devices(VkInstance);

	float score_device(VkPhysicalDevice);
	VkPhysicalDevice select_device(VkInstance);

	// page 12 add:
	VkDebugUtilsMessengerEXT create_debug_messenger(VkInstance);

	VKAPI_ATTR VkBool32 VKAPI_CALL debug_util_callback(
		VkDebugUtilsMessageSeverityFlagBitsEXT, VkDebugUtilsMessageTypeFlagsEXT,
		VkDebugUtilsMessengerCallbackDataEXT const*, void*);

	// page 15 add:
	std::optional<std::uint32_t> find_graphics_queue_family(VkPhysicalDevice);

	VkDevice create_device(VkPhysicalDevice, std::uint32_t aQueueFamily);


}

int main()
{
	std::printf("running ex1 main \n");
	// Use Volk to load the initial parts of the Vulkan API that are required
	// to create a Vulkan instance. This includes very few functions:
	// - vkGetInstanceProcAddr()
	// - vkCreateInstance()
	// - vkEnumerateInstanceExtensionProperties()
	// - vkEnumerateInstanceLayerProperties()
	// - vkEnumerateInstanceVersion() (added in Vulkan 1.1)
	if (auto const res = volkInitialize(); VK_SUCCESS != res)
	{
		std::fprintf(stderr, "Error: unable to load Vulkan API\n");
		std::fprintf(stderr, "Volk returned error %s\n", lut::to_string(res).c_str());
		return 1;
	}

	// We can use vkEnumerateInstanceVersion() to tell us the version of the
	// Vulkan loader. vkEnumerateInstanceVersion() was added in Vulkan 1.1, so
	// it might not be available on systems with particularly old Vulkan
	// loaders. In that case, assume the version is 1.0.x and output this.
	std::uint32_t loaderVersion = VK_MAKE_API_VERSION(0, 1, 0, 0);
	if (vkEnumerateInstanceVersion)
	{
		if (auto const res = vkEnumerateInstanceVersion(&loaderVersion); VK_SUCCESS != res)
		{
			std::fprintf(stderr, "Warning: vkEnumerateInstanceVersion() returned error %s\n", lut::to_string(res).c_str());
		}
	}

	std::printf("Vulkan loader version: %d.%d.%d (variant %d)\n", VK_API_VERSION_MAJOR(loaderVersion), VK_API_VERSION_MINOR(loaderVersion), VK_API_VERSION_PATCH(loaderVersion), VK_API_VERSION_VARIANT(loaderVersion));

	// adding in helpers
	// Check instance layers and extensions
	auto const supportedLayers = get_instance_layers();
	auto const supportedExtensions = get_instance_extensions();

	bool enableDebugUtils = false;
	std::vector<char const*> enabledLayers, enabledExensions;

#if !defined(NDEBUG) // debug builds only
	if (supportedLayers.count("VK_LAYER_KHRONOS_validation"))
	{
		enabledLayers.emplace_back("VK_LAYER_KHRONOS_validation");
	}

	if (supportedExtensions.count("VK_EXT_debug_utils"))
	{
		enableDebugUtils = true;
		enabledExensions.emplace_back("VK_EXT_debug_utils");
	}
#endif // ∼ debug builds


	// Print the names of the layers and extensions that we are enabling:
	for (auto const& layer : enabledLayers)
		std::printf("Enabling layer: %s\n", layer);

	for (auto const& extension : enabledExensions)
		std::printf("Enabling instance extension: %s\n", extension);

	// Create Vulkan instance
	//VkInstance instance = create_instance(); // old call
	VkInstance instance = create_instance(enabledLayers, enabledExensions,
		enableDebugUtils);

	if (VK_NULL_HANDLE == instance)
		return 1;

	// Instruct Volk to load the remainder of the Vulkan API.
	volkLoadInstance(instance);

	// Setup debug messenger // from page 13
	VkDebugUtilsMessengerEXT debugMessenger = VK_NULL_HANDLE;
	if (enableDebugUtils)
	{
		debugMessenger = create_debug_messenger(instance);
	}

	// Print Vulkan devices
	enumerate_devices(instance);

	// Select appropriate Vulkan device
	VkPhysicalDevice physicalDevice = select_device(instance);
	if (VK_NULL_HANDLE == physicalDevice)
	{
		vkDestroyInstance(instance, nullptr);

		std::fprintf(stderr, "Error: no suitable physical device found!\n");
		return 1;
	}

	{
		VkPhysicalDeviceProperties props;
		vkGetPhysicalDeviceProperties(physicalDevice, &props);
		std::printf("Selected device: %s\n", props.deviceName);
	}

	// Create a logical device // section 5 // page 16
	auto const graphicsFamilyIndex = find_graphics_queue_family(physicalDevice);
	if (!graphicsFamilyIndex)
	{
		vkDestroyInstance(instance, nullptr);
		std::fprintf(stderr, "Error: no graphics queue found\n");
		return 1;
	}
	VkDevice device = create_device(physicalDevice, *graphicsFamilyIndex);
	if (VK_NULL_HANDLE == device)
	{
		vkDestroyInstance(instance, nullptr);
		return 1;
	}

	// Optional: Specialize Vulkan functions for this device
	volkLoadDevice(device);

	// Retrieve VkQueue
	VkQueue graphicsQueue = VK_NULL_HANDLE;
	vkGetDeviceQueue(device, *graphicsFamilyIndex, 0, &graphicsQueue);

	assert(VK_NULL_HANDLE != graphicsQueue);

	// Cleanup

	vkDestroyDevice(device, nullptr);

	// added from page 14
	if (VK_NULL_HANDLE != debugMessenger)
		vkDestroyDebugUtilsMessengerEXT(instance, debugMessenger, nullptr);

	vkDestroyInstance(instance, nullptr);

	return 0;
}

namespace
{
	//VkInstance create_instance() { // old start
	VkInstance create_instance(std::vector<char const*> const& aEnabledLayers,
		std::vector<char const*> const& aEnabledExtensions,
		bool aEnableDebugUtils)
	{
		// Most of the VkApplicationInfo fields we can choose freely. The only 
		// ”important” field is the .apiVersion, which specifies the highest 
		// version of Vulkan that the application is designed to use. 

		VkApplicationInfo appInfo{};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "COMP5822-exercise1";
		appInfo.applicationVersion = 2021; // academic year of 2021/22 
		appInfo.apiVersion = VK_MAKE_API_VERSION(0, 1, 3, 0); // Version 1.3

		VkInstanceCreateInfo instanceInfo{};
		instanceInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		instanceInfo.pApplicationInfo = &appInfo;

		// adding enabled layers to instance info struct
		instanceInfo.enabledLayerCount = std::uint32_t(aEnabledLayers.size());
		instanceInfo.ppEnabledLayerNames = aEnabledLayers.data();

		instanceInfo.enabledExtensionCount = std::uint32_t(aEnabledExtensions.size());
		instanceInfo.ppEnabledExtensionNames = aEnabledExtensions.data();

		// debug validation checking // from page 14
		// 
		// If we wish to receive validation information on instance creation,
		// we need to provide additional information to	kCreateInstance(). This
		// is done by linking an instance of VkDebugUtilsMessengerCreateInfoEXT
		// into the pNext chain of VkInstanceCreateInfo.
		//
		// Note: this is identical to the one that we used when initializing
		// the debug utils ordinarily (see create debug messenger()).
		VkDebugUtilsMessengerCreateInfoEXT debugInfo{};
		if (aEnableDebugUtils)
		{
			debugInfo.sType =
				VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
			debugInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
				VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
			debugInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
				VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
				VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
			debugInfo.pfnUserCallback = &debug_util_callback;
			debugInfo.pUserData = nullptr;
			debugInfo.pNext = instanceInfo.pNext;
			instanceInfo.pNext = &debugInfo;
		}

		VkInstance instance = VK_NULL_HANDLE;

		if (auto const res = vkCreateInstance(&instanceInfo, nullptr, &instance);
			VK_SUCCESS != res) {

			std::fprintf(stderr, "Error: unable to create Vulkan instance\n");
			std::fprintf(stderr, "vkCreateInstance(): %s\n", lut::to_string(res).c_str());
			return VK_NULL_HANDLE;
		}

		return instance;
	}

	// helper functions
	std::unordered_set<std::string> get_instance_layers()
	{
		std::uint32_t numLayers = 0;
		if (auto const res = vkEnumerateInstanceLayerProperties(&numLayers, nullptr);
			VK_SUCCESS != res)
		{
			std::fprintf(stderr, "Error: unable to enumerate layers\n");
			std::fprintf(stderr, "vkEnumerateInstanceLayerProperties() returned % s\n",
				lut::to_string(res).c_str());
			return {};
		}

		std::vector<VkLayerProperties> layers(numLayers);
		if (auto const res = vkEnumerateInstanceLayerProperties(&numLayers, layers.data
		()); VK_SUCCESS != res)
		{
			std::fprintf(stderr, "Error: unable to get layer properties\n");
			std::fprintf(stderr, "vkEnumerateInstanceLayerProperties() returned % s\n",
				lut::to_string(res).c_str());
			return {};
		}

		std::unordered_set<std::string> res;
		for (auto const& layer : layers)
			res.insert(layer.layerName);

		return res;
	}

	std::unordered_set<std::string> get_instance_extensions()
	{
		std::uint32_t numExtensions = 0;
		if (auto const res = vkEnumerateInstanceExtensionProperties(NULL, &numExtensions, nullptr);
			VK_SUCCESS != res)
		{
			std::fprintf(stderr, "Error: unable to enumerate layers\n");
			std::fprintf(stderr, "vkEnumerateInstanceLayerProperties() returned % s\n",
				lut::to_string(res).c_str());
			return {};
		}

		std::vector<VkExtensionProperties> extensions(numExtensions);
		if (auto const res = vkEnumerateInstanceExtensionProperties(NULL, &numExtensions, extensions.data
		()); VK_SUCCESS != res)
		{
			std::fprintf(stderr, "Error: unable to get layer properties\n");
			std::fprintf(stderr, "vkEnumerateInstanceLayerProperties() returned % s\n",
				lut::to_string(res).c_str());
			return {};
		}

		std::unordered_set<std::string> res;
		for (auto const& extension : extensions)
			res.insert(extension.extensionName);

		return res;
	}

}



namespace
{
	float score_device(VkPhysicalDevice aPhysicalDev)
	{
		VkPhysicalDeviceProperties props;
		vkGetPhysicalDeviceProperties(aPhysicalDev, &props);
		// Only consider Vulkan 1.1 devices
		auto const major = VK_API_VERSION_MAJOR(props.apiVersion);
		auto const minor = VK_API_VERSION_MINOR(props.apiVersion);
		if (major < 1 || (major == 1 && minor < 1))
			return -1.f;
		// Discrete GPU  Integrated GPU > others
		float score = 0.f;
		if (VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU == props.deviceType)
			score += 500.f;
		else if (VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU == props.deviceType)
			score += 100.f;
		return score;
	}

	VkPhysicalDevice select_device(VkInstance aInstance)
	{
		assert(VK_NULL_HANDLE != aInstance);
		VkPhysicalDevice bestDevice = VK_NULL_HANDLE;

		// count num devices
		std::uint32_t numDevices = 0;
		// error catching if
		if (auto const res = vkEnumeratePhysicalDevices(aInstance, &numDevices, nullptr);
			VK_SUCCESS != res)
		{

			std::fprintf(stderr, "Error: unable to get phyiscal device count\n");
			std::fprintf(stderr, "vkEnumeratePhysicalDevices() returned error %s\n", lut::to_string(res).c_str());
			return bestDevice;
		}

		// init vector of devices
		std::vector<VkPhysicalDevice> devices(numDevices, VK_NULL_HANDLE);
		// error catching if
		if (auto const res = vkEnumeratePhysicalDevices(aInstance, &numDevices, devices.data());
			VK_SUCCESS != res)
		{

			std::fprintf(stderr, "Error: unable to get phyiscal device list\n");
			std::fprintf(stderr, "vkEnumeratePhysicalDevices() returned error %s\n", lut::to_string(res).c_str());
			return bestDevice;
		}

		std::printf("Found %zu devices:\n", devices.size());

		float bestScore = -1.f;


		for (auto const device : devices)
		{
			auto const score = score_device(device);
			if (score > bestScore)
			{
				bestScore = score;
				bestDevice = device;
			}
		}

		return bestDevice;
	}

	void enumerate_devices(VkInstance aInstance)
	{
		assert(VK_NULL_HANDLE != aInstance);

		// count num devices
		std::uint32_t numDevices = 0;
		// error catching if
		if (auto const res = vkEnumeratePhysicalDevices(aInstance, &numDevices, nullptr);
			VK_SUCCESS != res)
		{

			std::fprintf(stderr, "Error: unable to get phyiscal device count\n");
			std::fprintf(stderr, "vkEnumeratePhysicalDevices() returned error %s\n", lut::to_string(res).c_str());
			return;
		}

		// init vector of devices
		std::vector<VkPhysicalDevice> devices(numDevices, VK_NULL_HANDLE);
		// error catching if
		if (auto const res = vkEnumeratePhysicalDevices(aInstance, &numDevices, devices.data());
			VK_SUCCESS != res)
		{

			std::fprintf(stderr, "Error: unable to get phyiscal device list\n");
			std::fprintf(stderr, "vkEnumeratePhysicalDevices() returned error %s\n", lut::to_string(res).c_str());
			return;
		}

		std::printf("Found %zu devices:\n", devices.size());

		for (auto const device : devices) {
			VkPhysicalDeviceProperties props;
			vkGetPhysicalDeviceProperties(device, &props);

			auto const versionMajor = VK_API_VERSION_MAJOR(props.apiVersion);
			auto const versionMinor = VK_API_VERSION_MINOR(props.apiVersion);
			auto const versionPatch = VK_API_VERSION_PATCH(props.apiVersion);

			std::printf("- %s (Vulkan: %d.%d.%d, Driver: %s)\n", props.deviceName,
				versionMajor, versionMinor, versionPatch, lut::driver_version(props.vendorID,
					props.driverVersion).c_str());
			std::printf(" - Type: %s\n", lut::to_string(props.deviceType).c_str());

			// vkGetPhysicalDeviceFeatures2 is only available on devices with an 
			// API	ersion ≥ 1.1. Calling it on “older” devices is an error.
			if (versionMajor > 1 || (versionMajor == 1 && versionMinor >= 1))
			{
				VkPhysicalDeviceFeatures2 features{};
				features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;

				vkGetPhysicalDeviceFeatures2(device, &features);

				// See documentation for VkPhysicalDeviceFeatures2 and for the
				// original VkPhysicalDeviceFeatures (which is the ‘features’
				// member of the new	ersion) for a complete list of optional
				// features.
				//
				// In addition, several extensions deﬁne additional
				//vkPhysicalDevice* Features() structures that could be queried
				// as well (assuming the corresponding extension is supported).

				// Anisotropic ﬁltering is nice; one of the upcoming exercises
				// will use it (if possible):
				std::printf(" - Anisotropic filtering: %s\n", features.features.samplerAnisotropy ? "true" : "false");
			}

			std::uint32_t numQueues = 0;
			vkGetPhysicalDeviceQueueFamilyProperties(device, &numQueues, nullptr);

			std::vector<VkQueueFamilyProperties> families(numQueues);
			vkGetPhysicalDeviceQueueFamilyProperties(device, &numQueues, families.data());

			for (auto const& family : families)
			{
				std::printf(" - Queue family: %s (%u queues)\n", lut::queue_flags(family.
					queueFlags).c_str(), family.queueCount);
			}

			// Each device has a number of memory types and memory heaps. Heaps
			// correspond to a kind of memory, for example system memory or
			// on-device memory (VRAM). Sometimes, there are a few additional
			// special memory regions. Memory types use memory from one of the
			// heaps. When allocating memory, we select a memory type with the
			// desired properties (e.g., HOST VISIBLE = must be accessible from
			// the CPU).
			VkPhysicalDeviceMemoryProperties mem;
			vkGetPhysicalDeviceMemoryProperties(device, &mem);

			std::printf(" - %u heaps\n", mem.memoryHeapCount);;
			for (std::uint32_t i = 0; i < mem.memoryHeapCount; ++i)
			{
				std::printf(" - heap %2u: %6zu MBytes, %s\n", i, std::size_t(mem.memoryHeaps[i].size) / 1024 / 1024, lut::memory_heap_flags(mem.memoryHeaps[i].flags).c_str());
			}

			std::printf(" - %u memory types\n", mem.memoryTypeCount);
			for (std::uint32_t i = 0; i < mem.memoryTypeCount; ++i)
			{
				std::printf(" - type %2u: from heap %2u, %s\n", i, mem.memoryTypes[i].heapIndex, lut::memory_property_flags(mem.memoryTypes[i].propertyFlags).c_str());
			}
		}






	}
}

namespace { // debug messenger stuff
	VkDebugUtilsMessengerEXT create_debug_messenger(VkInstance aInstance)
	{
		assert(VK_NULL_HANDLE != aInstance);

		// Set up the debug messaging for the rest of the application
		VkDebugUtilsMessengerCreateInfoEXT debugInfo{};
		debugInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		debugInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		debugInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
			VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		debugInfo.pfnUserCallback = &debug_util_callback;
		debugInfo.pUserData = nullptr;

		VkDebugUtilsMessengerEXT messenger = VK_NULL_HANDLE;

		if (auto const res = vkCreateDebugUtilsMessengerEXT(aInstance, &debugInfo,
			nullptr, &messenger); VK_SUCCESS != res)
		{
			std::fprintf(stderr, "Error: unable to set up debug messenger\n");
			std::fprintf(stderr, "vkCreateDebugUtilsMessengerEXT() returned % s\n", lut::
				to_string(res).c_str());
			return VK_NULL_HANDLE;
		}

		return messenger;
	} // end create_debug_messenger

	VKAPI_ATTR VkBool32 VKAPI_CALL debug_util_callback(
		VkDebugUtilsMessageSeverityFlagBitsEXT aSeverity,
		VkDebugUtilsMessageTypeFlagsEXT aType, VkDebugUtilsMessengerCallbackDataEXT
		const* aData, void* /* aUserPtr */)
	{
		std::fprintf(stderr, "%s (%s): %s (%d)\n%s\n--\n", lut::to_string(aSeverity).
			c_str(), lut::message_type_flags(aType).c_str(), aData->pMessageIdName, aData->
			messageIdNumber, aData->pMessage);
		return VK_FALSE;
	}
}

namespace { // logical device stuff // section 5, page 14 on

	std::optional<std::uint32_t> find_graphics_queue_family(VkPhysicalDevice aPhysicalDev)
	{
		assert(VK_NULL_HANDLE != aPhysicalDev);

		std::uint32_t numQueues = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(aPhysicalDev, &numQueues, nullptr);

		std::vector<VkQueueFamilyProperties> families(numQueues);
		vkGetPhysicalDeviceQueueFamilyProperties(aPhysicalDev, &numQueues, families.data());

		for (std::uint32_t i = 0; i < numQueues; ++i)
		{
			auto const& family = families[i];
			if (VK_QUEUE_GRAPHICS_BIT & family.queueFlags)
				return i;
		}

		return {};
	}

	VkDevice create_device(VkPhysicalDevice aPhysicalDev, std::uint32_t aQueueFamily)
	{
		assert(VK_NULL_HANDLE != aPhysicalDev);
		float queuePriorities[1] = { 1.f };
		VkDeviceQueueCreateInfo queueInfo{};
		queueInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueInfo.queueFamilyIndex = aQueueFamily;
		queueInfo.queueCount = 1;
		queueInfo.pQueuePriorities = queuePriorities;
		VkPhysicalDeviceFeatures deviceFeatures{};
		// No extra features for now.

		VkDeviceCreateInfo deviceInfo{};
		deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceInfo.queueCreateInfoCount = 1;
		deviceInfo.pQueueCreateInfos = &queueInfo;
		deviceInfo.pEnabledFeatures = &deviceFeatures;
		VkDevice device = VK_NULL_HANDLE;
		if (auto const res = vkCreateDevice(aPhysicalDev, &deviceInfo, nullptr, &device);
			VK_SUCCESS != res)
		{
			std::fprintf(stderr, "Error: can’t create logical device\n");
			std::fprintf(stderr, "vkCreateDevice() returned % s\n", lut::to_string(res).
				c_str());
			return VK_NULL_HANDLE;
		}
		return device;
	}
}

//EOF vim:syntax=cpp:foldmethod=marker:ts=4:noexpandtab: 
